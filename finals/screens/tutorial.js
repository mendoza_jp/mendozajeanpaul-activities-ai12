import React, { useRef, useEffect } from 'react';
import { Text, View, StyleSheet, Animated, TouchableOpacity, StatusBar, Image } from 'react-native';
import * as Font from 'expo-font';



// FADE ANIMATION

const FadeInView = (props) => {
  const fadeAnim = useRef (new Animated.Value(0)).current

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 500,
        useNativeDriver: true
      } 
    ).start();
  },[fadeAnim])

  return (
    <Animated.View
    style={{
      ...props.style,
      opacity: fadeAnim,
    }}
  >
    {props.children}
  </Animated.View>
  );
}

const SlowFadeInView = (props) => {
  const fadeAnim = useRef (new Animated.Value(0)).current

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      } 
    ).start();
  },[fadeAnim])

  return (
    <Animated.View
    style={{
      ...props.style,
      opacity: fadeAnim,
    }}
  >
    {props.children}
  </Animated.View>
  );
}

const VerySlowFadeInView = (props) => {
  const fadeAnim = useRef (new Animated.Value(0)).current

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 3000,
        useNativeDriver: true
      } 
    ).start();
  },[fadeAnim])

  return (
    <Animated.View
    style={{
      ...props.style,
      opacity: fadeAnim,
    }}
  >
    {props.children}
  </Animated.View>
  );
}

const Lines = (words) => {
  return (
    <Text style={{ fontFamily: 'RetroGaming', fontSize: 12, lineHeight:18}}>{words.say}</Text>
  )
}



export default class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      tutorial: ['You will be given a situation.', 'Choose between 2 choices.', 'Every choice you take skips 5 years.', 'Choose carefully.','Your choices will write your story.'],
      doNoth: ['You do nothing.','You go back to sleep.'],
      callHelp: ['You call for help.','. . .','. . .', '*nothing happens*','You go back to sleep.'],
      empty: [],
      count: 0,
      x: 0,
      tut: 1,
      A1: 0,
    }
    this.fade
    this.newLine = <Text>{'\n'}</Text>
  }

  state = {
    fontsLoaded: false,
  };

  tutPress = () => {
    this.setState({
      count: this.state.count+1,
      empty: [...this.state.empty,this.state.tutorial[this.state.count]]
    })
  }

  helpPress = () => {
    this.setState({
        empty:[],
        A1: this.state.A1+1,
        x: 0
    })

  }

  notReady = () => {
    this.setState({
      tut: this.state.tut+1
    })
  }

  Ready = () => {
    this.setState({
      tut: this.state.tut+2
    })
  }

  async loadFonts() {
    await Font.loadAsync({
      // FONT IS HERE
      RetroGaming: require('../assets/fonts/RetroGaming.ttf'),
    });
    this.setState({ fontsLoaded: true });
  }

  componentDidMount() {
    this.loadFonts();
    StatusBar.setHidden(true);
  } 

  render() {
    var count = this.state.count;
    global.count = count;
    var tut = this.state.tut;

    
    // TUTORIAL
    if(tut==1&&count==0) {
      if (this.state.fontsLoaded) {
        return (
          <SlowFadeInView style={styles.container}>
            <View style={styles.container0}>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
                <View style={styles.piccontainer}>
                <Image style={{flex:1,width:'100%', height: undefined}} source={require('../assets/0.png')}/>
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.tutPress} activeOpacity={1} touchSoundDisabled={true}>
                <Text style={{fontFamily:'RetroGaming',color:'white'}}>TAP TO START</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={() => this.props.navigation.replace('C1')}>
                    <Text style={{fontFamily:'RetroGaming'}}>SKIP TUTORIAL</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            {/* <Text style={{ fontSize: 20 }}>Default Font</Text>
            <FadeInView style={{width:250, height:50, backgroundColor:'powderblue'}}>
            <Text style={{ fontFamily: 'RetroGaming', fontSize: 20 }}>RANDOM FADING IN TEXT</Text>
            </FadeInView> */}
  
          </SlowFadeInView>
        );
      } else {
        return null;
      }
    } else if (tut==1&&count>0&&count<6) {
      if (this.state.fontsLoaded) {
        return (
          <SlowFadeInView style={styles.container}>
            <View style={styles.container0}>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
               <Image style={{flex:1,width:'100%', height: undefined}} source={require('../assets/0.png')}/>
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.tutPress} activeOpacity={1} touchSoundDisabled={true}>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                }
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                </View>
              </View>
            </View>
          </SlowFadeInView>
        );
      } else {
        return null;
      }
    } else if (tut==1&&count>5) {
      if (this.state.fontsLoaded) {
        return (
          <View style={styles.container}>
            <View style={styles.container0}>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
                <View style={styles.piccontainer}>
                <Image style={{flex:1,width:'100%', height: undefined}} source={require('../assets/0.png')}/>
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.tutPress} activeOpacity={1} touchSoundDisabled={true}>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <View key={index}>
                        <Lines say={content}/>
                      </View>
                    )
                  })
                }
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                <TouchableOpacity style={styles.choicebox} onPress={this.notReady}>
                  <Text style={{fontFamily:'RetroGaming'}}>IM NOT READY</Text>
                </TouchableOpacity>
                <View style={styles.space}>
                </View>
                 {/* CHOICE2 */}
                <TouchableOpacity style={styles.choicebox} onPress={this.Ready}>
                  <Text style={{fontFamily:'RetroGaming'}}>IM READY</Text>
                </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </View>
        );
      } else {
        return null;
      }

    } else if (tut==2&&count>5) {
      if (this.state.fontsLoaded) {
        return (
          <View style={styles.container}>
            <View style={styles.container0}>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
                <View style={styles.piccontainer}>
                <Image style={{flex:1,width:'100%', height: undefined}} source={require('../assets/0.png')}/>
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={() => this.props.navigation.replace('C1')} activeOpacity={1} touchSoundDisabled={true}>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <View key={index}>
                        <Lines say={content}/>
                      </View>
                    )
                  })
                } 
                  <VerySlowFadeInView>
                    <Text style={{fontFamily:'RetroGaming', fontSize:12}}>You don't really have a choice here.</Text>
                  </VerySlowFadeInView>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                </View>
              </View>
            </View>
          </View>
        );
      } else {
        return null;
      }

    } else if (tut==3&&count>5) {
      if (this.state.fontsLoaded) {
        return (
          <View style={styles.container}>
            <View style={styles.container0}>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
                <View style={styles.piccontainer}>
                <Image style={{flex:1,width:'100%', height: undefined}} source={require('../assets/0.png')}/>
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={() => this.props.navigation.replace('C1')} activeOpacity={1} touchSoundDisabled={true}>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <View key={index}>
                        <Lines say={content}/>
                      </View>
                    )
                  })
                } 
                  <VerySlowFadeInView>
                    <Text style={{fontFamily:'RetroGaming', fontSize:12}}>Good luck.</Text>
                  </VerySlowFadeInView>

                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                </View>
              </View>
            </View>
          </View>
        );
      } else {
        return null;
      }

    }
  }
}

const styles = StyleSheet.create ({
  container: {
    flexDirection: 'column',
    flex:1,
    padding:1,
    backgroundColor: '#52543F'
  },
  container0: {
    flex:0.8,
    flexDirection: 'column-reverse',
    justifyContent:'space-between',
    alignItems:'flex-start'
  },
  container1: {
    flex:5,
    borderColor: 'black',
    borderWidth:1,
    backgroundColor: 'white'
  },
  piccontainer: {
    flex:1,
    backgroundColor:'#DADBEA'
  },
  textcontainer:{
    flex:1.5,
    backgroundColor:'black',
    opacity:0.9,
    alignItems:'center',
    justifyContent:'center'
  },
  textcontainer2:{
    flex:1.5,
    backgroundColor:'#DADBCE'
  },
  container2: {
    flex:1,
    justifyContent:'space-evenly',
    backgroundColor: '#DADBCE'
  },
  actioncontainer:{
    flex:1,
    padding:20,
    flexDirection:'row',
    alignContent: 'center',
    backgroundColor:'#52543F'
  },
  choicecontainer:{
    flex:1,
    flexDirection:'row',
    alignContent: 'center',
    backgroundColor:'#52543F'
  },
  choicebox: {
    flex:1,
    borderRadius:20,
    borderColor: 'black',
    borderWidth: 0.5,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#9A9E77'
  },
  choiceboxinvis: {
    flex:1,
    borderRadius:20,
    borderColor: 'black',
    justifyContent:'center',
    alignItems:'center',

  },
  space: {
    flex:0.5
  },
  continuebutton: {
    flex:1,
    flexDirection: 'column',
    padding: 30,
  },
  
})