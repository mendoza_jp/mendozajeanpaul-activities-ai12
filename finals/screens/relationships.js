import React, { useRef, useEffect } from 'react';
import { Text, TouchableOpacity, View, StyleSheet, Animated, Modal} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import * as Font from 'expo-font';

global.tabs = <Text>{'\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'}</Text>;
global.rscounts = 0;

//SELF
global.alive = true;
global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel fine.</Text>;
global.currentStatus = <Text style={{fontFamily:'RetroGaming',color:'green'}}>Healthy</Text>;

//FAMILY
global.Family = true;
global.FamilyR = <Text style={{fontFamily:'RetroGaming'}}>Family</Text>;
global.FamilyS = <Text style={{fontFamily:'RetroGaming',color:'gray'}}>NEUTRAL</Text>;

//MOTHER
global.Mother = true;
global.MotherR = <Text style={{fontFamily:'RetroGaming'}}>Mother</Text>;
global.MotherS = <Text style={{fontFamily:'RetroGaming',color:'green'}}>FRIENDLY</Text>;

//STEPDAD
global.Stepdad = false;
global.StepdadR = null;
global.StepdadS = null;

//STEPSIS
global.Hsis = false;
global.HsisR = null;
global.HsisS = null;

//DOG
global.Dog = false;
global.DogR = null;
global.DogS = null;

//FRANK
global.Frank = false;
global.FrankR = null;
global.FrankS = null;

//PARTNER
global.Partner = false;
global.PartnerR = null;
global.PartnerS = null;

//BUSINESS ASSOCIATES
global.busAs = false;
global.busAsR = null;
global.busAsS = null;

//CHILDREN???

//PUBLIC REPUTATION
global.publicRep = false;
global.publicRepR = null;
global.publicRepS = null;



const FastFadeInView = (props) => {
  const fadeAnim = useRef (new Animated.Value(0)).current

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 150,
        useNativeDriver: true
      } 
    ).start();
  },[fadeAnim])

  return (
    <Animated.View
    style={{
      ...props.style,
      opacity: fadeAnim,
    }}
  >
    {props.children}
  </Animated.View>
  );
}

const FadeInView = (props) => {
    const fadeAnim = useRef (new Animated.Value(0)).current
  
    useEffect(() => {
      Animated.timing(
        fadeAnim,
        {
          toValue: 1,
          duration: 500,
          useNativeDriver: true
        } 
      ).start();
    },[fadeAnim])
  
    return (
      <Animated.View
      style={{
        ...props.style,
        opacity: fadeAnim,
      }}
    >
      {props.children}
    </Animated.View>
    );
  }

const SlowFadeInView = (props) => {
  const fadeAnim = useRef (new Animated.Value(0)).current
  
  useEffect(() => {
    Animated.timing(
      fadeAnim,
        {
          toValue: 1,
          duration: 600,
          useNativeDriver: true
        } 
      ).start();
    },[fadeAnim])
  
  return (
    <Animated.View
    style={{
      ...props.style,
      opacity: fadeAnim,
    }}
    >
    {props.children}
  </Animated.View>
  );
}


const Lines = (words) => {    
  return (
    <Text style={{ fontFamily: 'RetroGaming', fontSize: 12, lineHeight:18}}>{words.say}</Text>
  )
}

export default class Relationships extends React.Component {

    constructor(props){
        super(props);
        this.state={
          modalDialogue:['You can check the status of your relationships in this screen.','Your feelings can also be seen in this screen.'],
          modalShown:0,
          count:global.rscounts,
          empty:[]
        }
        this.fade
    }

    state = {
        fontsLoaded: false,
        modalVisible: false
      };

      async loadFonts() {
        await Font.loadAsync({
          // FONT IS HERE
          RetroGaming: require('../assets/fonts/RetroGaming.ttf'),
        });
        this.setState({ fontsLoaded: true });
      }

      componentDidMount() {
        this.loadFonts();
      }

      addRsCount = () => {
        global.rscounts=global.rscounts+1;
      }

      setModalVisibile = (visible) => {
        this.setState({
          modalVisible: visible,
          empty: [...this.state.empty,this.state.modalDialogue[this.state.count+1]]
        });
        this.addRsCount()
      }

    render() {
      const {modalVisible} = this.state;

        if(global.rscounts==0) {
          if(this.state.fontsLoaded) {
            return (
                <FadeInView style={styles.container}>

                  <Modal 
                  animationType = {"fade"}
                  transparent ={true}
                  visible = {modalVisible}
                  onRequestClose = {()=> {this.setModalVisibile(!modalVisible)}}>
                  <View style={{backgroundColor:'#00000090',flex:1,justifyContent:'center'}}>
                    <FadeInView style={{flexDirection:'row',bottom:200}}>
                    <TouchableOpacity activeOpacity={1} style={{backgroundColor:'#DADBCE',flex:1, padding:10, borderRadius:20,borderColor:'black',borderWidth:1}} onPress={()=>{this.setModalVisibile(!modalVisible)}}>
                    <View style={{flexDirection:'column'}}>
                    <View>
                      <View>
                        <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'black'}}>{this.state.modalDialogue[0]}</Text>
                      </View>
                      {
                        this.state.empty.map((content,index) => {
                          return (
                            <FadeInView key={index}>
                              <Lines say={content}/>
                            </FadeInView>
                          )
                        })
                      } 
                    </View>
                    </View>
                    <FastFadeInView>
                      <Text style={{fontSize:12,lineHeight:18}}>Tap here to continue..</Text>
                    </FastFadeInView>
                    </TouchableOpacity>
                    </FadeInView>
                    </View>                
                   </Modal>

                    <View style={styles.container1}>
                        <View style={styles.customHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Icon name='arrow-undo-sharp' size={35} color='#DADBCE'/>
                        </TouchableOpacity>
                        </View>
                        <View style={styles.rsContainer}>
                        <View style={styles.rsNameContainer}>
                            <Text style={{fontFamily:'RetroGaming',fontSize:18}}>RELATIONSHIPS</Text>
                            {global.Family ? global.FamilyR : null}
                            {global.Mother ? global.MotherR : null}
                            {global.Stepdad ? global.StepdadR : null}
                            {global.Hsis ? global.HsisR : null}
                            {global.Dog ? global.DogR : null}
                            {global.Frank ? global.FrankR : null}
                            {global.Partner ? global.PartnerR : null}
                            {global.busAs ? global.busAsR : null}
                            {global.publicRep ? global.publicRepR : null}
                          </View>
                          <View style={styles.rsStatusContainer}>
                            <Text style={{fontFamily:'RetroGaming',fontSize:18}}>STATUS</Text>
                            {global.Family ? global.FamilyS : null}
                            {global.Mother ? global.MotherS : null}
                            {global.Stepdad ? global.StepdadS : null}
                            {global.Hsis ? global.HsisS : null}
                            {global.Dog ? global.DogS : null}
                            {global.Frank ? global.FrankS : null}
                            {global.Partner ? global.PartnerS : null}
                            {global.busAs ? global.busAsS : null}
                            {global.publicRep ? global.publicRepS : null}
                          </View>
                        </View>
                        <View style={styles.statusFeel}>
                          <View style={styles.feelsContainer}>
                            <Text style={{fontFamily:'RetroGaming',fontSize:18}}>FEELINGS</Text>
                            {global.alive ? global.feelings : <Text style={{fontFamily:'RetroGaming'}}>???</Text>}
                          </View>
                          <View style={styles.statusContainer}>
                            <Text style={{fontFamily:'RetroGaming',fontSize:18}}>STATUS</Text>
                            {global.alive ? global.currentStatus : <Text style={{fontFamily:'RetroGaming',color:'red'}}>DEAD</Text>}
                          </View>
                        </View>
                    </View>
                </FadeInView>
            )    
        } else {
            return null;
        }
        } else if (global.rscounts==1) {
          if(this.state.fontsLoaded) {
            return (
                <FadeInView style={styles.container}>

                  <Modal 
                  animationType = {"fade"}
                  transparent ={true}
                  visible = {modalVisible}
                  onRequestClose = {()=> {this.setModalVisibile(!modalVisible)}}>
                  <View style={{backgroundColor:'#00000090',flex:1,justifyContent:'center'}}>
                    <SlowFadeInView style={{flexDirection:'row',top:100}}>
                    <TouchableOpacity activeOpacity={1} style={{backgroundColor:'#DADBCE',flex:1, padding:10, borderRadius:20,borderColor:'black',borderWidth:1}} onPress={()=>{this.setModalVisibile(!modalVisible)}}>
                    <View style={{flexDirection:'column'}}>
                    <View>
                      {
                        this.state.empty.map((content,index) => {
                          return (
                            <View key={index}>
                              <Lines say={content}/>
                            </View>
                          )
                        })
                      } 
                    </View>
                    </View>
                    <View>
                      <Text style={{fontSize:12,lineHeight:18}}>Tap here to continue..</Text>
                    </View>
                    </TouchableOpacity>
                    </SlowFadeInView>
                    </View>                
                   </Modal>

                    <View style={styles.container1}>
                        <View style={styles.customHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Icon name='arrow-undo-sharp' size={35} color='#DADBCE'/>
                        </TouchableOpacity>
                        </View>
                        <View style={styles.rsContainer}>
                          <View style={styles.rsNameContainer}>
                            <Text style={{fontFamily:'RetroGaming',fontSize:18}}>RELATIONSHIPS</Text>
                            {global.Family ? global.FamilyR : null}
                            {global.Mother ? global.MotherR : null}
                            {global.Stepdad ? global.StepdadR : null}
                            {global.Hsis ? global.HsisR : null}
                            {global.Dog ? global.DogR : null}
                            {global.Frank ? global.FrankR : null}
                            {global.Partner ? global.PartnerR : null}
                            {global.busAs ? global.busAsR : null}
                            {global.publicRep ? global.publicRepR : null}
                          </View>
                          <View style={styles.rsStatusContainer}>
                            <Text style={{fontFamily:'RetroGaming',fontSize:18}}>STATUS</Text>
                            {global.Family ? global.FamilyS : null}
                            {global.Mother ? global.MotherS : null}
                            {global.Stepdad ? global.StepdadS : null}
                            {global.Hsis ? global.HsisS : null}
                            {global.Dog ? global.DogS : null}
                            {global.Frank ? global.FrankS : null}
                            {global.Partner ? global.PartnerS : null}
                            {global.busAs ? global.busAsS : null}
                            {global.publicRep ? global.publicRepS : null}
                          </View>
                        </View>
                        <View style={styles.statusFeel}>
                          <View style={styles.feelsContainer}>
                            <Text style={{fontFamily:'RetroGaming',fontSize:18}}>FEELINGS</Text>
                            {global.alive ? global.feelings : <Text style={{fontFamily:'RetroGaming'}}>???</Text>}
                          </View>
                          <View style={styles.statusContainer}>
                            <Text style={{fontFamily:'RetroGaming',fontSize:18}}>STATUS</Text>
                            {global.alive ? global.currentStatus : <Text style={{fontFamily:'RetroGaming',color:'red'}}>DEAD</Text>}
                          </View>
                        </View>
                    </View>
                </FadeInView>
            )    
        } else {
            return null;
        }
        } else if (global.rscounts>1) {
          if(this.state.fontsLoaded) {
            return (
                <FadeInView style={styles.container}>
                    <View style={styles.container1}>
                        <View style={styles.customHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Icon name='arrow-undo-sharp' size={35} color='#DADBCE'/>
                        </TouchableOpacity>
                        </View>
                        <View style={styles.rsContainer}>
                          <View style={styles.rsNameContainer}>
                            <Text style={{fontFamily:'RetroGaming',fontSize:18}}>RELATIONSHIPS</Text>
                            {global.Family ? global.FamilyR : null}
                            {global.Mother ? global.MotherR : null}
                            {global.Stepdad ? global.StepdadR : null}
                            {global.Hsis ? global.HsisR : null}
                            {global.Dog ? global.DogR : null}
                            {global.Frank ? global.FrankR : null}
                            {global.Partner ? global.PartnerR : null}
                            {global.busAs ? global.busAsR : null}
                            {global.publicRep ? global.publicRepR : null}
                          </View>
                          <View style={styles.rsStatusContainer}>
                            <Text style={{fontFamily:'RetroGaming',fontSize:18}}>STATUS</Text>
                            {global.Family ? global.FamilyS : null}
                            {global.Mother ? global.MotherS : null}
                            {global.Stepdad ? global.StepdadS : null}
                            {global.Hsis ? global.HsisS : null}
                            {global.Dog ? global.DogS : null}
                            {global.Frank ? global.FrankS : null}
                            {global.Partner ? global.PartnerS : null}
                            {global.busAs ? global.busAsS : null}
                            {global.publicRep ? global.publicRepS : null}
                          </View>
                        </View>
                        <View style={styles.statusFeel}>
                          <View style={styles.feelsContainer}>
                            <Text style={{fontFamily:'RetroGaming',fontSize:18}}>FEELINGS</Text>
                            {global.alive ? global.feelings : <Text style={{fontFamily:'RetroGaming'}}>???</Text>}
                          </View>
                          <View style={styles.statusContainer}>
                            <Text style={{fontFamily:'RetroGaming',fontSize:18}}>STATUS</Text>
                            {global.alive ? global.currentStatus : <Text style={{fontFamily:'RetroGaming',color:'red'}}>DEAD</Text>}
                          </View>
                        </View>
                    </View>
                </FadeInView>
            )    
        } else {
            return null;
        }
        }
    } 
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#52543F',
        alignContent: 'center',
        justifyContent: 'center',
        padding:20
    },
    container1: {
        flex: 1,
        flexDirection:'column',
        backgroundColor:'white',
        borderWidth:3,
        borderRadius:5
    },
    customHeader: {
        flex:0.5,
        padding:20,
        flexDirection:'row',
        backgroundColor:'#9A9E77',
        alignItems:'center',
        borderWidth:2
    },
    rsContainer: {
        flex:5,
        flexDirection: 'row',
        backgroundColor: '#DADBCE',
        padding:20,
        borderWidth: 2
    },
    rsNameContainer: {
        flex:2,
        backgroundColor:'#DADBCE',
        flexDirection:'column'
    },
    rsStatusContainer: {
      flex:1,
      backgroundColor:'#DADBCE',
      flexDirection:'column',
    },
    statusFeel: {
      flex:2,
      padding:10,
      backgroundColor:'#9A9E77',
      flexDirection: 'row',
      borderWidth:2,
      borderColor:'black'
    },
    feelsContainer: {
      flex:2,
      padding:10,
      backgroundColor:'#DADBCE'
    },
    statusContainer: {
      flex:1,
      padding:10,
      backgroundColor:'#DADBCE'
    }

})
