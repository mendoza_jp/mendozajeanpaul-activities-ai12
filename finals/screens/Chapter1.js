import React, { useRef, useEffect } from 'react';
import { Text, View, StyleSheet, Animated, TouchableOpacity, Modal, Button , StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import * as Font from 'expo-font';
import './relationships';
import '../choices';


// FADE ANIMATION
const FastFadeInView = (props) => {
    const fadeAnim = useRef (new Animated.Value(0)).current
  
    useEffect(() => {
      Animated.timing(
        fadeAnim,
        {
          toValue: 1,
          duration: 150,
          useNativeDriver: true
        } 
      ).start();
    },[fadeAnim])
  
    return (
      <Animated.View
      style={{
        ...props.style,
        opacity: fadeAnim,
      }}
    >
      {props.children}
    </Animated.View>
    );
  }

const FadeInView = (props) => {
  const fadeAnim = useRef (new Animated.Value(0)).current

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 500,
        useNativeDriver: true
      } 
    ).start();
  },[fadeAnim])

  return (
    <Animated.View
    style={{
      ...props.style,
      opacity: fadeAnim,
    }}
  >
    {props.children}
  </Animated.View>
  );
}

const FIView = (props) => {
  const fadeAnim = useRef (new Animated.Value(0)).current

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 500,
        useNativeDriver: true
      } 
    ).start();
  },[fadeAnim])

  return (
    <Animated.View
    style={{
      ...props.style,
      opacity: fadeAnim,
    }}
  >
    {props.children}
  </Animated.View>
  );
}

const SlowFadeInView = (props) => {
  const fadeAnim = useRef (new Animated.Value(0)).current

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      } 
    ).start();
  },[fadeAnim])

  return (
    <Animated.View
    style={{
      ...props.style,
      opacity: fadeAnim,
    }}
  >
    {props.children}
  </Animated.View>
  );
}

const VerySlowFadeInView = (props) => {
  const fadeAnim = useRef (new Animated.Value(0)).current

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 3000,
        useNativeDriver: true
      } 
    ).start();
  },[fadeAnim])

  return (
    <Animated.View
    style={{
      ...props.style,
      opacity: fadeAnim,
    }}
  >
    {props.children}
  </Animated.View>
  );
}

const Lines = (words) => {
  return (
    <Text style={{ fontFamily: 'RetroGaming', fontSize: 12, lineHeight:18}}>{words.say}</Text>
  )
}


export default class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      //CHOICE 1 DIALOGUE
      ADialogue: ['You are in the local park.','You see children playing.','You turn to your mother and she gives you a nod.',
                   'She probably wants you to play with the other kids.'],

      A1Dialogue: ['You go to the children and tried to befriend them.',"They didn't want to be your friend.",'You walked away.','...','...',
                    'A kid approaches you.','He tells you his name is Frank and that he loves pranks.','You hang out with Frank for the whole afternoon','Frank is now a FRIEND.'],
      A2Dialogue: ['You stay in place.','You look at your mother and she has a worried look on her face.','You play alone.','...','...',
                   'Your mother tells you its time to go home and both of you leave the park.','You find a lonely stray puppy on the way home.',"It doesn't seem to want to leave your side.",'You bring the puppy home.'],
      //CHOICE 2 DIALOGUE
      BADialogue: ['Your mother has been supportive with your friendship with Frank.','Your mom remarries.','Your new stepdad does not like Frank.','You and Frank have been pranking people for fun.','Your mom gets pregnant and gives birth to your new half-sister.',
                  'You are in school.','You and Frank plan to prank the new transfer student.', 'You see your target walking in the hallway.'],
      BBDialogue: ['Dogi becomes your bestfriend.','Your mother remarries.','Your stepdad likes Dogi.','Your dog cheers you up in days when you feel sad.','Your mom gets pregnant and gives birth to your new half-sister.',
                  'You are in school.','You found a group of bullies harassing your classmate right outside the school.','Your classmate looks at you, begging for help.'],
      B1Dialogue: ['You and Frank attempt to prank the transfer student.','Frank distracts the transfer student while you sneak up behind him.','You hear a bang,',"You see Frank drop to the ground, lifeless. The transfer student holding a gun towards Frank.",'Frank is dead.','A lot of people blame you for his death.','You transfer schools after the incident.'],
      B2Dialogue: ['You tell Frank that you have changed your mind.','Frank gets irritated and told you that you were no fun.',"Frank stops being your friend."],
      B3Dialogue: ['You report what you saw to the teachers.','The teachers catch the bullies redhanded.','The bullies were suspended.',"You are proud of what you did.",'You were then approached by bullies later on..'],
      B4Dialogue: ['You look into the eyes of your classmate and stood still.','You turn away and walk in the opposite direction, away from the scene.','A few weeks pass..',"You've noticed that your classmate hasn't been going to school for a while.",'The teacher has an announcement. He regrets to inform the class that your classmate has taken their life.'],
      //CHOICE 3 DIALOGUE
      CADialogue: ['Your family was affected by your decisions.','Your mother is sick.', 'Your half-sister is distant with you.','You are now in highschool.',"The drama of your previous issue has died down.",'You can start fresh.'],
      CBDialogue: ['You are lonely.','Your mother is sick.','Your half-sister likes spending time with you.','You transfer to a different school for highschool.','No one knows who you are.','You can start fresh.'],
      CCDialogue: ['You were beaten up by the bullies so bad that you were sent to the hospital.','Your vision has become impaired.','You were transferred to a different school.','Dogi is now almost 10 years old.','Your mother is sick.','Your half-sister is allergic to Dogi.','You are now in highschool.','You can start fresh.'],
      CDDialogue: ['You feel guilty.','Your mother is sick.','Dogi is now almost 10 years old.','Your half-sister is allergic to Dogi','You transfer to a different school for highschool.','No one knows who you are','You can start fresh.'],
      C1Dialogue: ['You talk to and befriend everyone.','You join every single social event that you could.','You get invited to multiple parties.','You meet Sofia in one of the parties and started dating her.','You make a name for yourself for being a party person.',
                  'You pass your classes, but you did not excel at it.','Your mother is proud of you.','Your stepfather dislikes your lifestyle.','Your half-sister is opening up to you.','The public has a positive impression on you.','You enroll in a public college.','Your life is well.'],
      C2Dialogue: ['You start studying.','You spend most of your time studying.','You miss out on social events.','You get high scores in quizzes and exams.','Your school made you participate in national quiz bees and competitions.','You graduate valedictorian and get a scholarship to a prestigious school.','Your mother is proud of you.','Your whole family attends your graduation ceremony.','Your half-sister gives you a gift.',
                  'The public respects you.','Your life is going well.','But you feel empty and lonely..'],
      C3Dialogue: ['You talk to and befriend everyone.','You join every single social event that you could.','You get invited to multiple parties.',"Frank was in one of the parties that you've attended and has been speaking ill of you.",'The people in the party have mixed feelings about you, but your friends defend your name.','You pass your classes, but you did not excel at it.',
                  'Your mother is proud of you.','Your stepfather is not impressed.','Your half-sister congratulates you.','The public respects you.','You enroll in a public college.','Your life is okay.'],
      C4Dialogue: ['You start studying.','You spend most of your time studying.','You miss out on social events.','You get high scores in quizzes and exams.','Your school made you participate in national quiz bees and competitions.','You graduate valedictorian and get a scholarship to a prestigious school.','Your mother is proud of you.','Your whole family attends your graduation ceremony.','Your half-sister gives you a gift.',
                  'You have heard from someone that Frank has been telling everybody that you were a tryhard.','The people defended you for being a man of dedication and shamed Frank for trying to make fun of you.','You are respected.'],
      C5Dialogue: ['You talk to and befriend everyone.','You join every single social event that you could.','You get invited to multiple parties.','You meet the classmate that you saved from the bullies. His name was David.','David was the host of the party and he spreads good word about you.','You pass your classes, but you did not excel at it. Though, a lot of people still congratulated and praised you.','Your mother is proud of you.','Your stepfather congratulates you.',
                  'Your half-sister congratulates you.','Your family congratulates you.','You enroll in a public college.','Dogi passes away at 14 years old.'],
      C6Dialogue: ['You start studying.','You spend most of your time studying. The rest of your time is spent on playing with Dogi.','You miss out on social events, though you are getting some recognition.','You get high scores in quizzes and exams.','Your school made you participate in national quiz bees and competitions.','You graduate valedictorian and get a scholarship to a prestigious school.','Your mother is proud.','Your whole family attends your graduation ceremony.','You meet the classmate that you saved from the bullies. His name was David. David was thankful for what you did.',
                  'David throws you a party in celebration of your success.','You are famous.','Dogi passes away at 14 years old.'],
      C7Dialogue: ['You talk to and befriend everyone.','You join every single social event that you could.','You get invited to multiple parties.','The guilt of your trauma has made you defend and save everyone who is in need of your help.','You become well-known for saving people in need. People often try to use and take advantage of you because of it.','You pass your classes, but you did not excel at it.',
                  'Your mother is proud of you.','Your stepfather is not amused.','Most of your friends only talk to you when they need you.','You feel sad.','Dogi passes away at 14 years old.','You show signs of depression'],
      C8Dialogue: ['You start studying.','You spend most of your time studying. The rest of your time is spent on playing with Dogi.','You miss out on social events.','The guilt of your trauma made has made you try to help everyone in need of help.','You become a target of bullying and people take advantage of you.','You graduate with high grades, but not enough for a scholarship to a prestigious college.','Your mother is proud of you.','Your family congratulates you but does not attend your graduation ceremony.',
                  'Your stepfather congratulates you.','Your half-sister is happy for you.','Dogi passes away at 14 years old.','You show signs of depression.'],
      //CHOICE 4 DIALOGUE
      DADialogue: ['You have been invited to the freshman college party.','You promised your girlfriend that you would go to the party with her.','Your peers and teachers expect you to attend the party.',"You drive to your girlfriend's house and park in the driveway. You see her walking towards your car, looking very excited.",'Your phone rings.','Its your stepdad.'],
      DBDialogue: ['You have been called to prepare yourself for an interview for the prestigious college.','You are confident you will pass the interview.','...','Your interview is today.','Your half-sister has called you and told you that your mother is dying.','Your interview is in 30 minutes.'],
      DCDialogue: ['You have been invited to the freshman college party.','You were told that the girl you like would be there.','...','Your friends tell you that Frank is at the party advancing on the girl you like.','You are on the way to party.','Your phone rings. Its your stepdad.'],
      DDDialogue: ['You have been called to prepare yourself for an interview for the prestigious college.','You are confident you will pass the interview.','...','Your interview is today.','Your half-sister has called you and told you that your mother is dying.','Your interview is in 30 minutes.'],
      DEDialogue: ['You have been invited to the freshman college party.','People and friends expect you to come to the party.','David tells you he has a business opportunity he wants to discuss during the party in case you were interested.','You are on the way to the party.','Your phone rings.','Its your stepdad.'],
      DFDialogue: ['You have been called to prepare yourself for an interview for the prestigious college.','You are confident you will pass the interview','...','Your interview is today.','Your half-sister has called you and told you that your mother is dying.','Your interview is in 30 minutes.'],
      DGDialogue: ['You have been invited to the freshman college party.','Your friends never mentioned that they would come to the party together with you.','You feel isolated and out of place. The silence is loud.','The silence is broken by the noises of people outside your house. Its your friends. They want you to come with them.','You are on the way to the party with your friends. Suddenly, your phone rings.','Its your stepdad.'],
      DHDialogue: ['You have been trying to get a scholarship for the past few weeks.','You finally get a call from a very good college and you have been accepted.','Your interview will be in an hour.','You clean yourself up and prepared your documents.','Your half-sister has called you and told you that your mother is dying.','You interview is in 30 minutes.'],
      D1Dialogue: ['You ignore the call.','You pick up your girlfriend and then head straight to the party.','You meet new people and had a wonderful time.','You meet people who would like to run a business with you in the near future.','Your girlfriend is having a great time.','Your night was memorable.'],
      D2Dialogue: ['You answer the call.','Your stepfather tells you that your mother is dying.',"Your girlfriend enters the car and tells you how happy she is that you're bringing her to the party.",'You break the news to your girlfriend.','Your girlfriend gets upset and exits the car.','You leave immediately.'],
      D3Dialogue: ['You leave for the interview.','Your half-sister and stepfather flood your phone with calls and messages.','You turn off your phone.','You arrive at the interview in time.','You did well in the interview.','Your mother has died.'],
      D4Dialogue: ['You leave for the hospital.','You have missed your college interview.','...',"You arrive at the hospital and rushed to find your mother's room.","You find the room and you walk inside to see your mother on her deathbed.",'Your mother smiles brightly as she sees you.'],
      D5Dialogue: ['You ignore the call and head straight to the party.','The people were excited to see you.','The girl you like ignores Frank and asks you out.','You meet people who would like to run a business with you in the near future.','You had a memorable night.','You had multiple missed calls and messages from your family.'],
      D6Dialogue: ['You answer the call.','Your stepdad tells you that your mother is dying.','You stop what you were doing and head straight to the hospital.','You get multiple missed calls and messages from your friends.',"You arrive at the hospital and found your mother's room. You walk inside to see your mother on her deathbed.",'Your mother smiles brightly as she sees you.'],
      D7Dialogue: ['You leave for the interview.','Your half-sister and stepfather flood your phone with calls and messages.','You turn off your phone.','You are on the way to your car. You see Frank carrying a bucket of paint to which he splatters all over you.','The paint gets all over your hair, face, and clothes.','You will not have enough time to clean up and go to the interview.'],
      D8Dialogue: ['You leave for the hospital.','You are on the way to your car. You see Frank carrying a bucket of paint to which he splatters all over you.','The paint gets all over your hair, face, and clothes.','Frank runs away. You head back inside to try to wash off the paint off your face and change your clothes.','You head straight to the hospital as fast as you can.','You arrive at the hospital.'],
      D9Dialogue: ['You ignore the call.','You head straight to the party.','Everybody cheered when they saw you.','You meet David and he introduced you to his business associates.','You had a fun night.','You had multiple missed calls and messages from your family.'],
      D10Dialogue: ['You answer the call.','Your stepfather tells you that your mother is dying.','You stop what you were doing and head straight to the hospital.','You get multiple missed calls and messages from your friends.',"You arrive at the hospital and found your mother's room. You walk inside to see your mother on her deathbed.",'Your mother smiles brightly as she sees you.'],
      D11Dialogue: ['You leave for the interview.','Your half-sister and stepfather flood your phone with calls and messages.','You turn off your phone.','You arrive at the interview in time.','Your interviewer knows who you are.',"She introduces herself as David's mother. You are going to get extra credit and favor in the prestigious college."],
      D12Dialogue: ['You leave for the hospital.','You have missed your college inerview.','...',"You arrive at the hospital and rushed to find your mother's room.","You find the room and you walk inside to see your mother on her deathbed.",'Your mother smiles brightly as she sees you.'],
      D13Dialogue: ['You ignore the call.','You head to the party with your friends.','Your friends open up to you and you open up to them.','You felt like you finally belong.','You had a wholesome night.','You had multiple missed calls and messages from your family.'],
      D14Dialogue: ['You answer the call.','Your stepfather tells you that you mother is dying.',"You had to tell your friends that you couldn't go with them because your mother is dying.",'You left immediately.',"You arrive at the hospital and found your mother's room. You walk inside to see your mother on her deathbed.",'Your mother smiles brightly as she sees you.'],
      D15Dialogue: ['You leave for the interview.','Your half-sister and stepfather flood your phone with calls and messages.','You turn off your phone.','You arrive at the interview in time.','You enter the interview room.','Your interviewer knows you.'],
      D16Dialogue: ['You leave for the hospital.','You have missed your last shot in getting into a respectable college.','...',"You arrive at the hospital and rushed to find your mother's room.","You find the room and you walk inside to see your mother on her deathbed.",'Your mother smiles brightly as she sees you.'],
      //ENDING DIALOGUES
      endingCH1AAAA: ['Your mother is dead.','Your stepdad told you that she wanted you see you before she died.','Your family disowned you and told you to never show your face again.','Your half-sister hates you.','You were forced to move out.','You had friends that would let you stay with them.','You worked a part-time job to support yourself.',"You finished college without honors. None of your family members showed up at your graduation ceremony."],
      endingCH1AAAB: ["You are at your mother's side with your half-sister.",'Your mother tells you that she loves you and to be good.','Your mother tells you not to worry.','Your mother dies happy.','You and your family mourns for your mother.','Your family gives you full support in your ventures.','Your girlfriend has not been the same since that night you let her down.','Your friends, family, and connections supported you during college to your graduation.'],
      endingCH1AABA: ['Your mother died looking for you.','Your family is upset with you.','Your stepfather is mad at you.','Your half-sister hates you.','You pass the college interview.','You were forced to move out and live in the school dormitories.','You finished college with high remarks. None of your family members attended your graduation ceremony.','You are alone.'],
      endingCH1AABB: ["You are at your mother's side with your half-sister.",'Your mother tells you that she loves you and to be good.','Your mother tells you not to worry.','Your mother dies happy.','You and your family mourns for your mother.','Your family gives you full support in your ventures.','You will not be given a second chance for your interview.','You enroll in a public college and graduated valedictorian.'],
      endingCH1ABAA: ['Your mother is dead.','Your stepdad is angry at you.','Your half-sister is upset with you.','Your family disowns you.','You were forced to move out.','Your friends got your back and let you stay with them.','You worked a part-time job to support yourself.','You finished college without honors. None of your family showed up at your graduation ceremony.'],
      endingCH1ABAB: ["You are at your mother's side with your half-sister.",'Your mother tells you that she loves you and to be good.','Your mother tells you not to worry.','Your mother dies happy.','You and your family mourns for your mother.','Your family gives you full support in your ventures.','Your friends prevented Frank from advancing on the girl you like.','Your friends, family, and connections supported you during college and you graduated with ease.'],
      endingCH1ABBA: ['Your mother died looking for you.','Your family is upset with you.','Your stepfather is mad at you.','Your half-sister hates you.','You were not able to go to the college interview.','You got the police to arrest Frank for assault from the video evidence from nearby CCTVs','You were forced by your family to move out.','You have nothing and have nowhere to go.'],
      endingCH1ABBB: ['You arrive at her hospital room to find out that your mother has already died.','Your family sees you and comforts you.','You have missed your college interivew.','Your family is upset with you.','Your stepfather is upset with you.','Your half-sister is mad at you.','You got the police to arrest Frank for assault from the video evidence from nearby CCTVs','You enroll in a public college and graduated valedictorian. Your stepfather showed up for you graduation ceremony.'],
      endingCH1BAAA: ['Your mother is dead.','Your stepdad is furious.','Your half-sister hates you.','Your family disowns you.','You were forced to move out.','David along with multiple other friends let you live with them.','You worked a part-time job to support yourself.','You finished college without honors. None of your family members showed up on the graduation ceremony.'],
      endingCH1BAAB: ["You are at your mother's side with your half-sister.",'Your mother tells you that she loves you and to be good.','Your mother tells you not to worry.','Your mother dies happy.','You and your family mourns for your mother.','Your family gives you full support in your ventures.','David covered you and told his business associates that you were busy.','Your friends, family, and connections supported you during college and you graduated with minimal effort. A business opportunity is ready for you to take.'],
      endingCH1BABA: ['Your mother died looking for you.','Your family is upset with you.','Your stepfather is upset with you.','Your half-sister hates you.','You were accepted into the prestigious college quickly.','You moved out of the house out of shame.','You lived in the college dormitories while you went to school.','You finished college with high remarks.'],
      endingCH1BABB: ["You are at your mother's side with your half-sister.",'Your mother tells you that she loves you and to be good.','Your mother tells you not to worry.','Your mother dies happy.','You and your family mourns for your mother.','Your family gives you full support in your ventures.','You missed your college interview, but David has helped you get a second chance.','Your new fame, connections, and wide ranges of opportunity has enabled you to graduate valedictorian. A bright future is ahead of you.'],
      endingCH1BBAA: ['Your mother is dead.','Your stepdad is furious.','Your half-sister hates you.','Your family disowns you.','You were forced to move out and was told to never show yourself to them again.',"Your friends let you stay with them until you were able to get back on your feet.",'You worked a part-time job to support yourself.','You finished college with the help of your friends. None of your family members showed up on the graduation cermony.'],
      endingCH1BBAB: ["You are at your mother's side with your half-sister.",'Your mother tells you that she loves you and to be good.','Your mother tells you not to worry.','Your mother dies happy.','You and your family mourns for your mother.','Your family gives you full support in your ventures.','Your friends was understanding of your situation and gave their condolences.','Your friends and family supported you during college to your graduation. Your family attend your graduation ceremony.'],
      endingCH1BBBA: ["Your interviewer was the mother of the bully victim you ignored. Your name was written in your classmate's suicide note all those years ago.",'Your interviewer is bitter about what happened, but she cannot fail you for personal reasons. You are accepted, but will not have a great reputation.','Your mother died looking for you.','Your whole family either hates or is mad at you.','You are disowned by your family.','You are forced to move out and live in the college dormitories.','You have no friends and family to help you.','You graduated college. None of your family members attended the ceremony.'],
      endingCH1BBBB: ["You are at your mother's side with your half-sister.",'Your mother tells you that she loves you and to be good.','Your mother tells you not to worry.','Your mother dies happy.','You and your family mourns for your mother.','Your family gives you full support in your ventures.','You enrolled in a public college.','You graduated with high remarks. Your family attends your ceremony.'],
      modalDialogue: ['You have unlocked RELATIONSHIPS.', 'They change based on your decisions.'], 
      empty: [],
      empty2: [],
      count: 0,
      count2:0,
      A1:0,
      A2:0,
      A3:0,
      A4:0,
      end:0,
    }
    this.fade
  }

  state = {
    fontsLoaded: false,
    modalVisible: false
  };

  // THIS CLEARS ALL CHOICES
  newGame = () =>{
  this.nextLineA();
  global.ch1A = null;
  global.ch1B = null;
  global.ch1AA = null;
  global.ch1AB = null;
  global.ch1BB = null;
  global.ch1BA = null;
  global.ch1AAA = null; 
  global.ch1BAA = null;
  global.ch1ABA = null;
  global.ch1AAB = null;
  global.ch1ABB = null;
  global.ch1BAB = null;
  global.ch1BBA = null;
  global.ch1BBB = null;
  global.ch1AAAA = null;
  global.ch1BAAB = null;
  global.ch1ABAA = null;
  global.ch1AABB = null;
  global.ch1ABBA = null;
  global.ch1BABB = null;
  global.ch1BBAA = null;
  global.ch1BBBB = null;
  global.ch1AAAB = null;
  global.ch1BAAA = null;
  global.ch1ABAB = null;
  global.ch1AABA = null;
  global.ch1ABBB = null;
  global.ch1BABA = null;
  global.ch1BBAB = null;
  global.ch1BBBA = null;

  //RS CLEAR
  global.rscounts = 0;
  //SELF
global.alive = true;
global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel fine.</Text>;
global.currentStatus = <Text style={{fontFamily:'RetroGaming',color:'green'}}>Healthy</Text>;

//FAMILY
global.Family = true;
global.FamilyR = <Text style={{fontFamily:'RetroGaming'}}>Family</Text>;
global.FamilyS = <Text style={{fontFamily:'RetroGaming',color:'gray'}}>NEUTRAL</Text>;

//MOTHER
global.Mother = true;
global.MotherR = <Text style={{fontFamily:'RetroGaming'}}>Mother</Text>;
global.MotherS = <Text style={{fontFamily:'RetroGaming',color:'green'}}>FRIENDLY</Text>;

//STEPDAD
global.Stepdad = false;
global.StepdadR = null;
global.StepdadS = null;

//STEPSIS
global.Hsis = false;
global.HsisR = null;
global.HsisS = null;

//DOG
global.Dog = false;
global.DogR = null;
global.DogS = null;

//FRANK
global.Frank = false;
global.FrankR = null;
global.FrankS = null;

//PARTNER
global.Partner = false;
global.PartnerR = null;
global.PartnerS = null;

//BUSINESS ASSOCIATES
global.busAs = false;
global.busAsR = null;
global.busAsS = null;

//CHILDREN???

//PUBLIC REPUTATION
global.publicRep = false;
global.publicRepR = null;
global.publicRepS = null;
}

  retryGame = () => {
    this.newGame();
    this.setState({
      empty: [],
      empty2: [],
      count: 0,
      count2:0,
      A1:0,
      A2:0,
      A3:0,
      A4:0,
      end:0,
    })
  }

  // THIS PROGRESSES THE STORY BY CHANGING THE DIALOGUE 
  nextLineA = () => {
    if (this.state.A1==0) {
      this.setState({
        count: this.state.count+1,
        empty: [...this.state.empty,this.state.ADialogue[this.state.count]]
      })
    }
    if (this.state.A1==1&&global.ch1A==true) {
      this.setState({
        count: this.state.count+1,
        empty: [...this.state.empty,this.state.A1Dialogue[this.state.count+1]]
      })
    }
    if (this.state.A1==1&&global.ch1B==true) {
      this.setState({
        count: this.state.count+1,
        empty: [...this.state.empty,this.state.A2Dialogue[this.state.count+1]]
        })
    } 


    console.log('count:',this.state.count)
  }

  nextLineB = () => {
    // BADIALOGUE
    if (this.state.A2==0&&global.ch1A==true&&this.state.count<5) {
      this.setState({
        count: this.state.count+1
      })
      if (this.state.count==0) {
        global.Stepdad=true;
        global.StepdadR = <Text style={{fontFamily:'RetroGaming'}}>Stepdad</Text>;
        global.StepdadS = <Text style={{fontFamily:'RetroGaming',color:'gray'}}>NEUTRAL</Text>; 
      }

      if (this.state.count==2) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel happy.</Text>;
      }

      if (this.state.count==3) {
        global.Hsis=true;
        global.HsisR = <Text style={{fontFamily:'RetroGaming'}}>Half-Sister</Text>;
        global.HsisS = <Text style={{fontFamily:'RetroGaming',color:'gray'}}>NEUTRAL</Text>; 
      }


    }
    // BBDIALOGUE
    if (this.state.A2==0&&global.ch1B==true&&this.state.count<5) {
      if (this.state.count==0) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel content.</Text>;
        global.DogS = <Text style={{fontFamily:'RetroGaming', color:'#cf1965'}}>LOVED</Text>;
      }

      if (this.state.count==1) {
        global.Stepdad=true;
        global.StepdadR = <Text style={{fontFamily:'RetroGaming'}}>Stepdad</Text>;
        global.StepdadS = <Text style={{fontFamily:'RetroGaming',color:'gray'}}>NEUTRAL</Text>; 
      }

      if (this.state.count==4) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel happy.</Text>;
      }

      if (this.state.count==4) {
        global.Hsis=true;
        global.HsisR = <Text style={{fontFamily:'RetroGaming'}}>Half-Sister</Text>;
        global.HsisS = <Text style={{fontFamily:'RetroGaming',color:'gray'}}>NEUTRAL</Text>; 
      }


      this.setState({
        count: this.state.count+1
      })
    }

    //nextlineb CH1A
    if (this.state.A2==0&&global.ch1A==true&&this.state.count>4&&this.state.count<7) {
      this.setState({
        count: this.state.count+1,
        empty: [...this.state.empty,this.state.BADialogue[this.state.count+1]]
      })
    }
    // CH1B
    if (this.state.A2==0&&global.ch1B==true&&this.state.count>4&&this.state.count<7) {
      if (this.state.count==0) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel scared.</Text>;
      }

      this.setState({
        count: this.state.count+1,
        empty: [...this.state.empty,this.state.BBDialogue[this.state.count+1]]
      })
    }

    //nextlineB1 prank choice CH1AA B1
    if (this.state.A2==1&&global.ch1AA==true) {
      //B1DIALOGUE
      if (this.state.count==5) {
        global.FrankR = <Text style={{fontFamily:'RetroGaming'}}>Frank✞</Text>;
        global.FrankS = <Text style={{fontFamily:'RetroGaming',color:'#4f4f4f'}}>DEAD</Text>;

        //SELF
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel anxious, grief, and guilt.</Text>;
        global.currentStatus = <Text style={{fontFamily:'RetroGaming',color:'#c7991a'}}>DISTURBED</Text>
      }

      this.setState({
        count: this.state.count+1
      })
    }

    // reconsider CH1AB B2
    if (this.state.A2==1&&global.ch1AB==true) {
      //B2DIALOGUE
      if (this.state.count==0) {
        global.FrankS = <Text style={{fontFamily:'RetroGaming',color:'#c7991a'}}>IRRITATED</Text>;
      }
      if (this.state.count==2) {
        global.FrankR = <Text style={{fontFamily:'RetroGaming',color:'#96091e'}}>Frank</Text>;
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel sad.</Text>;
      }

      this.setState({
        count: this.state.count+1,
        empty: [...this.state.empty,this.state.B2Dialogue[this.state.count+1]]
      })
    }
    //nextlineB2 report B3DIALOGUE
    if (this.state.A2==1&&global.ch1BA==true) {
      if (this.state.count==2) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are proud.</Text>;
      }

      this.setState({
        count: this.state.count+1,
        empty: [...this.state.empty,this.state.B3Dialogue[this.state.count+1]]
      })
    }

    // B4DIALOGUE
    if (this.state.A2==1&&global.ch1BB==true) {
      if (this.state.count==0) {
        global.currentStatus = <Text style={{fontFamily:'RetroGaming',color:'#820021'}}>GUILTY</Text>;
      }

      this.setState({
        count: this.state.count+1,
        empty: [...this.state.empty,this.state.B4Dialogue[this.state.count+1]]
      })
    }

    console.log('Count: ',this.state.count)
  }

  nextLineC = () => {
    //rs adder
    if (global.ch1AA==true) {
      //CADIALOGUE
      if (this.state.A3==0) {
        if (this.state.count==0) {
          global.FamilyS = <Text style={{fontFamily:'RetroGaming', color:'#c7991a'}}>IRRITATED</Text>;
        }
        if (this.state.count==1) {
          global.MotherR = <Text style={{fontFamily:'RetroGaming',color:'#546e4d'}}>Mother</Text>;
        }

        if (this.state.count==2) {
          global.HsisS = <Text style={{fontFamily:'RetroGaming'}}>DISTANT</Text>;
        }

        if (this.state.count==3) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You want to be better.</Text>;
        }
      }

      //C1DIALOGUE
      if(this.state.A3==1&&ch1AAA==true) {
        if (this.state.count==0) {
          global.publicRep=true;
          global.publicRepR= <Text style={{fontFamily:'RetroGaming'}}>Public Reputation</Text>;
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'gray'}}>NEUTRAL</Text>;
        }

        if (this.state.count==3) {
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'#7bc21d'}}>KNOWN</Text>;
        }

        if (this.state.count==4) {
          global.Partner = true;
          global.PartnerR = <Text style={{fontFamily:'RetroGaming'}}>Sofia</Text>;
          global.PartnerS = <Text style={{fontFamily:'RetroGaming',color:'#b8407b'}}>CLOSE</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel in love.</Text>;
        }

        if (this.state.count==5) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel happy.</Text>;
        }

        if (this.state.count==7) {
            global.StepdadS= <Text style={{fontFamily:'RetroGaming', color:'#c7991a'}}>IRRITATED</Text>;
        }

        if (this.state.count==8) {
          global.HsisS= <Text style={{fontFamily:'RetroGaming', color:'green'}}>FRIENDLY</Text>;
        }

        if (this.state.count==9) {
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'#13a867'}}>WELL-KNOWN</Text>;
        }

        if (this.state.count==10) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel great.</Text>;
        }

      }
      //C2DIALOGUE
      if(this.state.A3==1&&ch1AAB==true) {
        if (this.state.count==3) {
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'gray'}}>UNKNOWN</Text>;
        }

        if (this.state.count==4) {
          global.Partner = true;
          global.PartnerR = <Text style={{fontFamily:'RetroGaming'}}>Sofia</Text>;
          global.PartnerS = <Text style={{fontFamily:'RetroGaming',color:'#b8407b'}}>CLOSE</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel in love.</Text>;
        }

        if (this.state.count==5) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel successful.</Text>;
        }

        if (this.state.count==7) {
            global.FamilyS= <Text style={{fontFamily:'RetroGaming', color:'#05a615'}}>PROUD</Text>;
        }

        if (this.state.count==8) {
          global.HsisS= <Text style={{fontFamily:'RetroGaming', color:'green'}}>FRIENDLY</Text>;
        }

        if (this.state.count==9) {
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'#2da649'}}>RESPECTED</Text>;
        }

        if (this.state.count==10) {
          global.currentStatus = <Text style={{fontFamily:'RetroGaming',color:'#42005c'}}>DEPRESSED</Text>;
        }
      }

      this.setState({
        count: this.state.count+1
      })
  
      console.log('count: ',this.state.count)

    }

    if (global.ch1AB==true) {
      //CBDIALOGUE
      if (this.state.A3==0) {
        if (this.state.count==0) {
          global.currentStatus = <Text style={{fontFamily:'RetroGaming'}}>LONELY</Text>;
        }
        if (this.state.count==1) {
          global.MotherR = <Text style={{fontFamily:'RetroGaming',color:'#546e4d'}}>Mother</Text>;
        }

        if (this.state.count==2) {
          global.HsisS = <Text style={{fontFamily:'RetroGaming',color:'green'}}>FRIENDLY</Text>;
        }

        if (this.state.count==3) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You want to be better.</Text>;
        }
      }

      //C3DIALOGUE
      if(this.state.A3==1&&ch1ABA==true) {
        if (this.state.count==0) {
          global.publicRep=true;
          global.publicRepR= <Text style={{fontFamily:'RetroGaming'}}>Public Reputation</Text>;
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'gray'}}>NEUTRAL</Text>;
        }

        if (this.state.count==2) {
          global.currentStatus = <Text style={{fontFamily:'RetroGaming',color:'green'}}>HEALTHY</Text>;
        }

        if (this.state.count==3) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel annoyed.</Text>;
          global.FrankS= <Text style={{fontFamily:'RetroGaming', color:'#961302'}}>HOSTILE</Text>;
        }

        if (this.state.count==7) {
            global.StepdadS= <Text style={{fontFamily:'RetroGaming', color:'#c7991a'}}>IRRITATED</Text>;
        }

        if (this.state.count==8) {
          global.HsisS= <Text style={{fontFamily:'RetroGaming', color:'green'}}>FRIENDLY</Text>;
        }

        if (this.state.count==9) {
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'#2da649'}}>RESPECTED</Text>;
        }

        if (this.state.count==10) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel okay.</Text>;
        }

      }


      //C4DIALOGUE
      if(this.state.A3==1&&ch1ABB==true) {
        if (this.state.count==3) {
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'gray'}}>UNKNOWN</Text>;
        }

        if (this.state.count==5) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel successful.</Text>;
        }

        if (this.state.count==7) {
            global.FamilyS= <Text style={{fontFamily:'RetroGaming', color:'#05a615'}}>PROUD</Text>;
        }

        if (this.state.count==9) {
          global.FrankS = <Text style={{fontFamily:'RetroGaming', color:'#961302'}}>HOSTILE</Text>
          global.feelings = <Text style={{fontFamily:'RetroGaming',}}>You are annoyed.</Text>
        }

        if (this.state.count==10) {
          global.feelings = <Text style={{fontFamily:'RetroGaming',}}>You feel happy.</Text>
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'#2da649'}}>RESPECTED</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming',}}>You feel content.</Text>
        }

      }

      this.setState({
        count: this.state.count+1
      })
  
      console.log('count: ',this.state.count)

    }

    if (global.ch1BA==true) {
      //CCDIALOGUE
      if (this.state.A3==0) {
        if (this.state.count==0) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are in pain.</Text>;
        }
        if (this.state.count==4) {
          global.MotherR = <Text style={{fontFamily:'RetroGaming',color:'#546e4d'}}>Mother</Text>;
        }

        if (this.state.count==5) {
          global.HsisS = <Text style={{fontFamily:'RetroGaming',color:'green'}}>DISTANT</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You want change.</Text>;
        }

      }

      //C5DIALOGUE
      if(this.state.A3==1&&ch1BAA==true) {
        if (this.state.count==2) {
          global.publicRep=true;
          global.publicRepR= <Text style={{fontFamily:'RetroGaming'}}>Public Reputation</Text>;
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'#7bc21d'}}>KNOWN</Text>;
        }

        if (this.state.count==3) {
          global.Partner = true;
          global.PartnerR = <Text style={{fontFamily:'RetroGaming'}}>David</Text>;
          global.PartnerS = <Text style={{fontFamily:'RetroGaming',color:'green'}}>FRIENDLY</Text>;
        }

        if (this.state.count==4) {
          global.publicRepS = <Text style={{fontFamily:'RetroGaming',color:'#2da649'}}>RESPECTED</Text>;
        }

        if (this.state.count==5) {
          global.publicRepS = <Text style={{fontFamily:'RetroGaming',color:'#2a7d54'}}>PRAISED</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel happy.</Text>;
        }

        if (this.state.count==7) {
          global.StepdadS= <Text style={{fontFamily:'RetroGaming', color:'green'}}>FRIENDLY</Text>;
          global.HsisS= <Text style={{fontFamily:'RetroGaming', color:'gray'}}>NEUTRAL</Text>;
        }

        if (this.state.count==8) {
          global.FamilyS= <Text style={{fontFamily:'RetroGaming', color:'green'}}>FRIENDLY</Text>;
        }

        if (this.state.count==10) {
          global.DogR = <Text style={{fontFamily:'RetroGaming'}}>Dogi✞</Text>;
          global.DogS = <Text style={{fontFamily:'RetroGaming',color:'#4f4f4f'}}>DEAD</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel sad.</Text>;
        }
      }

      //C6DIALOGUE
      if(this.state.A3==1&&ch1BAB==true) {
        if (this.state.count==2) {
          global.publicRep=true;
          global.publicRepR= <Text style={{fontFamily:'RetroGaming'}}>Public Reputation</Text>;
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'#7bc21d'}}>KNOWN</Text>;
        }

        if (this.state.count==5) {
          global.publicRepS = <Text style={{fontFamily:'RetroGaming',color:'#2da649'}}>RESPECTED</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel successful.</Text>;
        }

        if (this.state.count==7) {
          global.FamilyS = <Text style={{fontFamily:'RetroGaming',color:'#05a615'}}>PROUD</Text>;
          global.HsisS = <Text style={{fontFamily:'RetroGaming',color:'green'}}>FRIENDLY</Text>;
          global.StepdadS = <Text style={{fontFamily:'RetroGaming',color:'green'}}>FRIENDLY</Text>;
        }

        if (this.state.count==10) {
          global.publicRepS = <Text style={{fontFamily:'RetroGaming', color:'#a8077b'}}>FAMOUS</Text>;
          global.DogR = <Text style={{fontFamily:'RetroGaming'}}>Dogi✞</Text>;
          global.DogS = <Text style={{fontFamily:'RetroGaming',color:'#4f4f4f'}}>DEAD</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel sad.</Text>;
        }

      }

      this.setState({
        count: this.state.count+1
      })
  
      console.log('count: ',this.state.count)
      

    }

    if (global.ch1BB==true) {
      //CDDIALOGUE
      if (this.state.A3==0) {
        if (this.state.count==0) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are guilty.</Text>;
        }
        if (this.state.count==1) {
          global.MotherR = <Text style={{fontFamily:'RetroGaming',color:'#546e4d'}}>Mother</Text>;
        }

        if (this.state.count==3) {
          global.HsisS = <Text style={{fontFamily:'RetroGaming',color:'green'}}>DISTANT</Text>;
        }

        if (this.state.count==4) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You want change.</Text>;
        }
      }

      //C7DIALOGUE
      if(this.state.A3==1&&ch1BBA==true) {
        if (this.state.count==0) {
          global.publicRep=true;
          global.publicRepR= <Text style={{fontFamily:'RetroGaming'}}>Public Reputation</Text>;
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'gray'}}>UNKNOWN</Text>;
        }

        if (this.state.count==2) {
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'#7bc21d'}}>KNOWN</Text>;
        }

        if (this.state.count==3) {
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'#13a867'}}>WELL-KNOWN</Text>;
        }

        if (this.state.count==4) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel guilty.</Text>;
        }

        if (this.state.count==5) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel okay.</Text>;
        }

        if (this.state.count==8) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel used and lonely.</Text>;
        }

        if (this.state.count==9) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel sad.</Text>;
        }

        if (this.state.count==10) {
          global.DogR = <Text style={{fontFamily:'RetroGaming'}}>Dogi✞</Text>;
          global.DogS = <Text style={{fontFamily:'RetroGaming',color:'#4f4f4f'}}>DEAD</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel sad.</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are depressed.</Text>;
          global.currentStatus = <Text style={{fontFamily:'RetroGaming',color:'#42005c'}}>DEPRESSED</Text>;
        }

      }

      //C8DIALOGUE
      if(this.state.A3==1&&ch1BBB==true) {
        if (this.state.count==3) {
          global.publicRep=true;
          global.publicRepR= <Text style={{fontFamily:'RetroGaming'}}>Public Reputation</Text>;
          global.publicRepS= <Text style={{fontFamily:'RetroGaming', color:'gray'}}>UNKNOWN</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel guilty.</Text>;
        }

        if (this.state.count==4) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel upset.</Text>;
        }

        if (this.state.count==5) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel used and lonely.</Text>;
        }

        if (this.state.count==7) {
          global.FamilyS = <Text style={{fontFamily:'RetroGaming',color:'#c7991a'}}>OKAY</Text>;
        }

        if (this.state.count==7) {
          global.StepdadS = <Text style={{fontFamily:'RetroGaming',color:'green'}}>FRIENDLY</Text>;
        }

        if (this.state.count==9) {
          global.HsisS = <Text style={{fontFamily:'RetroGaming',color:'#c7991a'}}>OKAY</Text>;
        }

        if (this.state.count==10) {
          global.DogR = <Text style={{fontFamily:'RetroGaming'}}>Dogi✞</Text>;
          global.DogS = <Text style={{fontFamily:'RetroGaming',color:'#4f4f4f'}}>DEAD</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel sad.</Text>;
          global.currentStatus = <Text style={{fontFamily:'RetroGaming', color:'#42005c'}}>DEPRESSED</Text>;
        }

      }

      this.setState({
        count: this.state.count+1
      })
      console.log('count: ',this.state.count)
    }

  }

  // NEXTLINED
  nextLineD = () => {
    // DADIALOGUE
    if (this.state.A4==0&&global.ch1AAA==true) {
      if (this.state.count==0) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel excited.</Text>;
      }

      this.setState({
        count: this.state.count+1,
        empty:[...this.state.empty,this.state.DADialogue[this.state.count+1]]
      })
    }

    // DBDIALOGUE
    if (this.state.A4==0&&global.ch1AAB==true) {
      if (this.state.count==1) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel confident.</Text>;
      }

      if (this.state.count==4) {
        global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel scared.</Text>;
      }

      this.setState({
        count: this.state.count+1,
        empty:[...this.state.empty,this.state.DBDialogue[this.state.count+1]]
      })
    }

    // DCDIALOGUE
    if (this.state.A4==0&&global.ch1ABA==true) {
      if(this.state.count==0) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel excited.</Text>;
      }

      if(this.state.count==3) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel anxious.</Text>;
      }

      if(this.state.count==4) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are in a hurry.</Text>;
      }

      this.setState({
        count: this.state.count+1,
        empty:[...this.state.empty,this.state.DCDialogue[this.state.count+1]]
    })
    }

    // DDDIALOGUE
    if (this.state.A4==0&&global.ch1ABB==true) {

      if(this.state.count==1) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel confident.</Text>;
      }

      if(this.state.count==4) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel scared.</Text>;
      }

      this.setState({
        count: this.state.count+1,
        empty:[...this.state.empty,this.state.DDDialogue[this.state.count+1]]
    })
    }

    // DEDIALOGUE
    if (this.state.A4==0&&global.ch1BAA==true) {
      if(this.state.count==0) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel excited.</Text>;
      }

      if(this.state.count==2) {
        global.busAs = true;
        global.busAsR = <Text style={{fontFamily:'RetroGaming'}}>Business Associates</Text>;
        global.busAsS = <Text style={{fontFamily:'RetroGaming',color:'gray'}}>NEUTRAL</Text>;

      }

      if(this.state.count==4) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are in a hurry.</Text>;
      }

      this.setState({
        count: this.state.count+1,
        empty:[...this.state.empty,this.state.DEDialogue[this.state.count+1]]
      })
    }

    // DFDIALOGUE
    if (this.state.A4==0&&global.ch1BAB==true) {
      if(this.state.count==0) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are excited.</Text>;
      }

      if(this.state.count==1) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are ready.</Text>;
      }

      if(this.state.count==3) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are scared.</Text>;
      }

      this.setState({
        count: this.state.count+1,
        empty:[...this.state.empty,this.state.DFDialogue[this.state.count+1]]
      })
    }

    // DGDIALOGUE
    if (this.state.A4==0&&global.ch1BBA==true) {
      if(this.state.count==0) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are excited.</Text>;
      }

      if(this.state.count==1) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You realize you are lonely.</Text>;
      }

      if(this.state.count==2) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are overthinking.</Text>;
      }

      if(this.state.count==3) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are surprised and cheered up.</Text>;
      }

      if(this.state.count==4) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You want to be happy.</Text>;
      }


      this.setState({
        count: this.state.count+1,
        empty:[...this.state.empty,this.state.DGDialogue[this.state.count+1]]
      })
    }

    // DHDIALOGUE
    if (this.state.A4==0&&global.ch1BBB==true) {
      if(this.state.count==1) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>Your hopes are up.</Text>;
      }

      if(this.state.count==3) {
        global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You are scared.</Text>;
      }

      this.setState({
        count: this.state.count+1,
        empty:[...this.state.empty,this.state.DHDialogue[this.state.count+1]]
      })
    }


    if (this.state.A4==1) {
      //D1DIALOGUE
      if(global.ch1AAAA==true) {
        if (this.state.count==2) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel amazing.</Text>;
        }
        if (this.state.count==3) {
          global.busAs = true;
          global.busAsR = <Text style={{fontFamily:'RetroGaming'}}>Business Assiociates</Text>;
          global.busAsS = <Text style={{fontFamily:'RetroGaming',color:'gray'}}>NEUTRAL</Text>;
        }
      }

      //D2DIALOGUE
      if(global.ch1AAAB==true) {
        if (this.state.count==2) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel scared and shocked.</Text>;
        }
        if (this.state.count==5) {
          global.PartnerS = <Text style={{fontFamily:'RetroGaming',color:'#29407d'}}>UPSET</Text>;
          }
      }

      //D3DIALOGUE
      if (global.ch1AABA==true) {
        if (this.state.count==0) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel terrible.</Text>;
        }

        if (this.state.count==1) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel terrible.</Text>;
          global.StepdadS= <Text style={{fontFamily:'RetroGaming', color:'#29407d'}}>UPSET</Text>;
          global.HsisS= <Text style={{fontFamily:'RetroGaming', color:'#29407d'}}>UPSET</Text>;
          global.FamilyS= <Text style={{fontFamily:'RetroGaming', color:'#29407d'}}>UPSET</Text>;
        }
        
        if (this.state.count==2) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel shame.</Text>;
        }

        if (this.state.count==4) {
          global.MotherR = <Text style={{fontFamily:'RetroGaming'}}>Mother✞</Text>;
          global.MotherS = <Text style={{fontFamily:'RetroGaming',color:'#4f4f4f'}}>DEAD</Text>;
          global.currentStatus = <Text style={{fontFamily:'RetroGaming',color:'#42005c'}}>DEPRESSED</Text>;
        }
      }

      //D4DIALOGUE
      if (global.ch1AABB==true) {

        if (this.state.count==1) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You are upset.</Text>;
        }
        
        if (this.state.count==4) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You are afraid.</Text>;
        }

        if (this.state.count==4) {
          global.MotherS = <Text style={{fontFamily:'RetroGaming',color:'#099692'}}>HAPPY</Text>;
          global.feelings = <Text style={{fontFamily:'RetroGaming',color:'#099692'}}>You are glad.</Text>
        }
      }

      //D5DIALOGUE
      if (global.ch1ABAA==true) {
        if (this.state.count==0) {
          global.StepdadS = <Text style={{fontFamily:'RetroGaming', color:'#29407d'}}>UPSET</Text>;
        }

        if (this.state.count==1) {
          global.publicRep = <Text style={{fontFamily:'RetroGaming', color:'#2a7d54'}}>PRAISED</Text>;
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You are happy.</Text>;

        }
        
        if (this.state.count==4) {
          global.StepdadS= <Text style={{fontFamily:'RetroGaming', color:'red'}}>ANGRY</Text>;
          global.HsisS= <Text style={{fontFamily:'RetroGaming', color:'#29407d'}}>UPSET</Text>;
          global.FamilyS= <Text style={{fontFamily:'RetroGaming', color:'#961302'}}>HOSTILE</Text>;
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel shame.</Text>;
        }

      }

      //D6DIALOGUE
      if (global.ch1ABAB==true) {

        if (this.state.count==1) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel scared.</Text>;
        }

        if (this.state.count==4) {
          global.MotherS = <Text style={{fontFamily:'RetroGaming',color:'#099692'}}>HAPPY</Text>;
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel glad.</Text>;
        }
      }

      //D7DIALOGUE
      if (global.ch1ABBA==true) {
        if (this.state.count==0) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel terrible.</Text>;
        }

        if (this.state.count==1) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel guilty.</Text>;
          global.StepdadS= <Text style={{fontFamily:'RetroGaming', color:'red'}}>ANGRY</Text>;
          global.HsisS= <Text style={{fontFamily:'RetroGaming', color:'#29407d'}}>UPSET</Text>;
          global.FamilyS= <Text style={{fontFamily:'RetroGaming', color:'#961302'}}>HOSTILE</Text>;
        }

        if (this.state.count==3) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel shame.</Text>;
        }

        if (this.state.count==4) {
          global.feelings = <Text style={{fontFamily:'RetroGaming'}}>You feel hopeless.</Text>;
          global.currentStatus = <Text style={{fontFamily:'RetroGaming', color:'#42005c'}}>MISERABLE</Text>;
        }
      }      

      //D8DIALOGUE
      if (global.ch1ABBB==true) {
        if (this.state.count==0) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You are in a hurry.</Text>;
        }

        if (this.state.count==1) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel irritated.</Text>;

        }
      }

      //D9DIALOGUE
      if (global.ch1BAAA==true) {
        if (this.state.count==0) {
          global.StepdadS =<Text style={{fontFamily:'RetroGaming',color:'#29407d'}}>UPSET</Text>;
        }

        if (this.state.count==1) {
          global.publicRepS =<Text style={{fontFamily:'RetroGaming',color:'#a8077b'}}>FAMOUS</Text>;
        }

        if (this.state.count==3) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel happy.</Text>;
        }

        if (this.state.count==4) {
          global.busAsS =<Text style={{fontFamily:'RetroGaming',color:'#a8077b'}}>RESPECTED</Text>;
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel shame.</Text>;
          global.StepdadS =<Text style={{fontFamily:'RetroGaming',color:'red'}}>ANGRY</Text>;
          global.Hsis =<Text style={{fontFamily:'RetroGaming',color:'#29407d'}}>UPSET</Text>;
          global.FamilyS =<Text style={{fontFamily:'RetroGaming',color:'#29407d'}}>UPSET</Text>;
        }

      }        

      //D10DIALOGUE
      if (global.ch1BAAB==true) {
        if (this.state.count==0) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel happy.</Text>;
        }

        if (this.state.count==3) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel anxious.</Text>;
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel glad.</Text>;
          global.MotherS =<Text style={{fontFamily:'RetroGaming',color:'#099692'}}>HAPPY</Text>;
        }

      }
      
      //D11DIALOGUE
      if (global.ch1BABA==true) {
        if (this.state.count==0) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel terrible.</Text>;
        }

        if (this.state.count==1) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel guilty.</Text>;
          global.StepdadS =<Text style={{fontFamily:'RetroGaming',color:'#29407d'}}>UPSET</Text>;
          global.HsisS =<Text style={{fontFamily:'RetroGaming',color:'#29407d'}}>UPSET</Text>;
          global.FamilyS =<Text style={{fontFamily:'RetroGaming',color:'#29407d'}}>UPSET</Text>;
          global.MotherS =<Text style={{fontFamily:'RetroGaming',color:'#29407d'}}>UPSET</Text>;
        }
        
        if (this.state.count==2) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel shame.</Text>;
        }
      }  

      //D12DIALOGUE
      if (global.ch1BABB==true) {
        if (this.state.count==0) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel anxious.</Text>;
        }

        if (this.state.count==1) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel sad.</Text>;
        }

        if (this.state.count==4) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel scared.</Text>;
          global.MotherS =<Text style={{fontFamily:'RetroGaming',color:'#099692'}}>HAPPY</Text>;
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel glad.</Text>;   
        }
        
      }   

      //D13DIALOGUE
      if (global.ch1BBAA==true) {
        if (this.state.count==1) {
          global.StepfatherS =<Text style={{fontFamily:'RetroGaming',color:'#29407d'}}>UPSET</Text>;
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel happy.</Text>;
        }

        if (this.state.count==2) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel great.</Text>;
        }

        if (this.state.count==4) {
          global.currentStatus =<Text style={{fontFamily:'RetroGaming',color:'#c7991a'}}>OKAY</Text>;
          global.StepfatherS =<Text style={{fontFamily:'RetroGaming',color:'red'}}>ANGRY</Text>;
          global.HsisS =<Text style={{fontFamily:'RetroGaming',color:'#29407d'}}>UPSET</Text>;
          global.FamilyS =<Text style={{fontFamily:'RetroGaming',color:'#29407d'}}>UPSET</Text>;
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel shame.</Text>;   
        }

      }   

      //D14DIALOGUE
      if (global.ch1BBAB==true) {
        if (this.state.count==1) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel scared.</Text>;
        }

        if (this.state.count==4) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel anxious.</Text>;
          global.MotherS =<Text style={{fontFamily:'RetroGaming',color:'#099692'}}>HAPPY</Text>;
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel glad.</Text>;     
          global.currentStatus =<Text style={{fontFamily:'RetroGaming',color:'#c7991a'}}>OKAY</Text>;  
        }
        
      }  

      //D15DIALOGUE
      if (global.ch1BBBA==true) {
        if (this.state.count==0) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel terrible.</Text>;
        }

        if (this.state.count==1) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel guilty.</Text>;
          global.StepdadS =<Text style={{fontFamily:'RetroGaming',color:'red'}}>ANGRY</Text>;
          global.HsisS =<Text style={{fontFamily:'RetroGaming',color:'#29407d'}}>UPSET</Text>;
          global.FamilyS =<Text style={{fontFamily:'RetroGaming',color:'red'}}>ANGRY</Text>;
        }

        if (this.state.count==2) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel shame.</Text>;
        }

      } 

      //D16DIALOGUE
      if (global.ch1BBBB==true) {
        if (this.state.count==0) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel sad.</Text>;
        }

        if (this.state.count==1) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel upset.</Text>;
        }

        if (this.state.count==4) {
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You feel anxious.</Text>;
          global.feelings =<Text style={{fontFamily:'RetroGaming'}}>You are anxious.</Text>;
          global.MotherS =<Text style={{fontFamily:'RetroGaming',color:'#099692'}}>HAPPY</Text>;
        }
        
      } 

      //THIS ADDS 1 TO COUNT
      this.setState({
        count: this.state.count+1
      })
    }
    

    if (this.state.end==1) {
      this.setState({
        count: this.state.count+1
      })
    }
    console.log('count:',this.state.count)
  }

  // CHOICES
  //CH1A
  makeFriends = () => {
    this.setState({
      A1: this.state.A1+1, 
      count: 0,
      empty:[]
    })
    global.ch1A=true;
  }

  //CH1B
  playAlone = () => {
    this.setState({
      A1: this.state.A1+1,
      count:0,
      empty:[]
    })
    global.ch1B=true;
  }

  //CH1AA
  prank = () => {
    this.setState({
      A2: this.state.A2+1,
      count:0,
      empty:[]
    })
    global.ch1AA=true;
  }

  //CH1AB
  reconsider = () => {
    this.setState({
      A2: this.state.A2+1,
      count:0,
      empty:[]
    })
    global.ch1AB=true;
  }

  //CH1BA
  report = () => {
    this.setState({
      A2: this.state.A2+1,
      count:0,
      empty:[]
    })
    global.ch1BA=true;
  }

  //CH1BB
  ignore = () => {
    this.setState({
      A2: this.state.A2+1,
      count:0,
      empty:[]
    })
    global.ch1BB=true;
  }

  //CH1AAA
  makeConnections = () => {
    this.setState({
      A3: this.state.A3+1,
      count:0,
      empty:[]
    })
    if (global.ch1AA==true) {
      global.ch1AAA=true;
    }
    if (global.ch1AB==true) {
      global.ch1ABA=true;
    }
    if (global.ch1BA==true) {
      global.ch1BAA=true;
    }
    if (global.ch1BB==true) {
      global.ch1BBA=true;
    }
  }

  //CH1AAB
  study = () => {
    this.setState({
      A3: this.state.A3+1,
      count:0,
      empty:[]
    })
    if (global.ch1AA==true) {
      global.ch1AAB=true;
    }
    if (global.ch1AB==true) {
      global.ch1ABB=true;
    }
    if (global.ch1BA==true) {
      global.ch1BAB=true;
    }
    if (global.ch1BB==true) {
      global.ch1BBB=true;
    }
  }

  //CH1AAAA
  ignoreCall = () => {
    this.setState({
      A4: this.state.A4+1,
      count:0,
      empty:[]
    })

    if (global.ch1AAA==true) {
      global.ch1AAAA=true;
    }

    if (global.ch1ABA==true) {
      global.ch1ABAA=true;
    }

    if (global.ch1BAA==true) {
      global.ch1BAAA=true;
    }

    if (global.ch1BBA==true) {
      global.ch1BBAA=true;
    }
  }
  //CH1AAAB
  answerCall = () => {
    this.setState({
      A4: this.state.A4+1,
      count:0,
      empty:[]
    })

    if (global.ch1AAA==true) {
      global.ch1AAAB=true;
    }
    if (global.ch1ABA==true) {
      global.ch1ABAB=true;
    }

    if (global.ch1BAA==true) {
      global.ch1BAAB=true;
    }

    if (global.ch1BBA==true) {
      global.ch1BBAB=true;
    }

  }
  //CH1AABA
  goInterview = () => {
    this.setState({
      A4: this.state.A4+1,
      count:0,
      empty:[]
    })
    if (global.ch1AAB==true) {
      global.ch1AABA=true;
    }
    if (global.ch1ABB==true) {
      global.ch1ABBA=true;
    }
    if (global.ch1BAB==true) {
      global.ch1BABA=true;
    }
    if (global.ch1BBB==true) {
      global.ch1BBBA=true;
    }
  }
  //CH1AABB
  goMom = () => {
    this.setState({
      A4: this.state.A4+1,
      count:0,
      empty:[]
    })
    
    if (global.ch1AAB==true) {
      global.ch1AABB=true;
    }
    if (global.ch1ABB==true) {
      global.ch1ABBB=true;
    }
    if (global.ch1BAB==true) {
      global.ch1BABB=true;
    }
    if (global.ch1BBB==true) {
      global.ch1BBBB=true;
    }
  }

  clearAll = () => {
    this.setState({
      empty:[],
      count: 0
    })
  }

  choice1DONE = () => {
    //CHANGE NUMBER TO 9 WHEN A CHOICE HAS BEEN CHOSEN
    this.setState({
      A1:9,
    })
    this.clearAll();
  }

  choice2DONE = () => {
    this.setState({
      A2:9
    })
    this.clearAll();
  }

  choice3DONE = () => {
    this.setState({
      A3: 9
    })
    this.clearAll();
  }

  choice4DONE = () => {
    this.setState({
      A4:9,
      end:1
    })
    this.clearAll();
  }

  // THIS ADDS RELATIONSHIPS

  frankFriend = () => {
    global.Frank = true;
    global.FrankR = <Text style={{fontFamily:'RetroGaming'}}>Frank</Text>;
    global.FrankS = <Text style={{fontFamily:'RetroGaming',color:'green'}}>FRIENDLY</Text>
  }

  dogiFriend = () => {
    global.Dog = true;
    global.DogR = <Text style={{fontFamily:'RetroGaming'}}>Dogi</Text>;
    global.DogS = <Text style={{fontFamily:'RetroGaming',color:'green'}}>FRIENDLY</Text>
  }

  pressFrank =() => {
    this.frankFriend();
    this.nextLineA();
  }

  pressDogi =() => {
    this.dogiFriend();
    this.nextLineA();
  }

  async loadFonts() {
    await Font.loadAsync({
      // FONT IS HERE
      RetroGaming: require('../assets/fonts/RetroGaming.ttf'),
    });
    this.setState({ fontsLoaded: true });
  }

  componentDidMount() {
    this.loadFonts();
    StatusBar.setHidden(true);
  } 

  setModalVisibile = (visible) => {
    this.setState({
      modalVisible: visible,
      count2: this.state.count2+1,
      empty2: [...this.state.empty2,this.state.modalDialogue[this.state.count2+1]]
    });
  }
  
  render() {
    var count = this.state.count;
    var count2 = this.state.count2;
    var A1 = this.state.A1;
    var A2 = this.state.A2;
    var A3 = this.state.A3;
    var A4 = this.state.A4;
    var end = this.state.end;
    const {modalVisible} = this.state;
    
    // CHOICE 1
    if(A1==0&&count==0) {
      if (this.state.fontsLoaded) {
        return (
          <SlowFadeInView style={styles.container}>
            <View style={styles.container0}>
              <View style={styles.rsIconContainer}>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 5</Text>
                </VerySlowFadeInView>
              </View>
            
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.newGame} activeOpacity={1} touchSoundDisabled={true}>
                <Text style={{fontFamily:'RetroGaming',color:'white'}}>CHAPTER 1</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis} onPress={this.helpPress}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </SlowFadeInView>
        );
      } else {
        return null;
      }
    } else if (A1==0&&count>0&&count<5) {
      if (this.state.fontsLoaded) {
        return (
          <SlowFadeInView style={styles.container}>
            <View style={styles.container0}>
              <View style={styles.rsIconContainer}>

              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 5</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineA} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </SlowFadeInView>
        );
      } else {
        return null;
      }
    } else if (A1==0&&count>4) {
      if (this.state.fontsLoaded) {
        return (
          <SlowFadeInView style={styles.container}>
            <View style={styles.container0}>
              <View style={styles.rsIconContainer}>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 5</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineA} activeOpacity={1} touchSoundDisabled={true}>
                <View>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <View key={index}>
                        <Lines say={content}/>
                      </View>
                    )
                  })
                } 
                </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <SlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.makeFriends}>
                    <Text style={{fontFamily:'RetroGaming'}}>MAKE FRIENDS</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.playAlone}>
                    <Text style={{fontFamily:'RetroGaming'}}>PLAY ALONE</Text>
                  </TouchableOpacity>
                </SlowFadeInView>
              </View>
            </View>
          </SlowFadeInView>
        );
      } else {
        return null;
      }
    } 
    // MAKE FRIENDS CHOICE (CH1A)
    else if (A1==1&&count>=0&&count<=7&&global.ch1A==true) {
      if (this.state.fontsLoaded) {
        return (
          <SlowFadeInView style={styles.container}>
            <View style={styles.container0}>
              <View style={styles.rsIconContainer}>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={{fontFamily:'RetroGaming',fontSize:25,color:'#DADBCE'}}>AGE 5</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineA} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.A1Dialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <SlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </SlowFadeInView>
              </View>
            </View>
            {/* <Text style={{ fontSize: 20 }}>Default Font</Text>
            <FadeInView style={{width:250, height:50, backgroundColor:'powderblue'}}>
            <Text style={{ fontFamily: 'RetroGaming', fontSize: 20 }}>RANDOM FADING IN TEXT</Text>
            </FadeInView> */}
  
          </SlowFadeInView>
        );
      } else {
        return null;
      }
    } else if (A1==1&&count==8&&global.ch1A==true) {
      if (this.state.fontsLoaded) {
        return (
          <SlowFadeInView style={styles.container}>
            <View style={styles.container0}>
              <View style={styles.rsIconContainer}>
                <FadeInView>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={this.pressFrank}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </FadeInView>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 5</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.pressFrank} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.A1Dialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <SlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </SlowFadeInView>
              </View>
            </View>
          </SlowFadeInView>
        );
      } else {
        return null;
      }
    }  else if (A1==1&&count==9&&global.ch1A==true) {
      if (this.state.fontsLoaded) {
        return (
          <SlowFadeInView style={styles.container}>
            <VerySlowFadeInView>
              <Modal 
              animationType = {"fade"}
              transparent ={true}
              visible = {modalVisible}
              statusBarTranslucent={true}
              onRequestClose = {()=> {this.setModalVisibile(!modalVisible)}}>
              <View style={{backgroundColor:'#00000090',flex:1,flexDirection:'column'}}>
                <View style={{flexDirection:'row',flex:2,alignItems:'center',}}>
                <View style={{flexDirection:'row'}}>
                <View style={{padding:10,opacity:0}}>
                  <Icon name="heart" size={50}/>
                </View>
                <TouchableOpacity activeOpacity={1} style={{backgroundColor:'#DADBCE', padding:10, borderRadius:20,borderColor:'black',flex:1,borderWidth:1}} onPress={()=>{this.setModalVisibile(!modalVisible)}}>
                <View style={{flexDirection:'column'}}>
                <FadeInView>
                  <FadeInView style={{flexDirection:'row'}}>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'black'}}>{count2==0 ? <Text>{this.state.modalDialogue[0]}</Text> : <FadeInView style={{flexDirection:'row',flex:1}}><Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'black',flex:1}}>{this.state.modalDialogue[1]}</Text></FadeInView>}</Text>
                  </FadeInView>
                </FadeInView>
                </View>
                <FastFadeInView>
                  <Text style={{fontSize:12,lineHeight:18}}>Tap here to continue..</Text>
                </FastFadeInView>
                </TouchableOpacity>
                </View>

                </View>

                <View style={{flex:8}}>

                </View>
              </View>                
              </Modal>
            </VerySlowFadeInView>

            <View style={styles.container0}>
              <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 5</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice1DONE} activeOpacity={1} touchSoundDisabled={true}>
                <View>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.A1Dialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <View key={index}>
                        <Lines say={content}/>
                      </View>
                    )
                  })
                } 
                </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <SlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </SlowFadeInView>
              </View>
            </View>
          </SlowFadeInView>
        );
      } else {
        return null;
      }
    } 

    // PLAY ALONE CHOICE (CH1B)
    else if (A1==1&&count>=0&&count<=7&&global.ch1B==true) {
      if (this.state.fontsLoaded) {
        return (
          <SlowFadeInView style={styles.container}>
            <View style={styles.container0}>
              <View style={styles.rsIconContainer}>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={{fontFamily:'RetroGaming',fontSize:25,color:'#DADBCE'}}>AGE 5</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineA} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.A2Dialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <SlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </SlowFadeInView>
              </View>
            </View>
            {/* <Text style={{ fontSize: 20 }}>Default Font</Text>
            <FadeInView style={{width:250, height:50, backgroundColor:'powderblue'}}>
            <Text style={{ fontFamily: 'RetroGaming', fontSize: 20 }}>RANDOM FADING IN TEXT</Text>
            </FadeInView> */}
  
          </SlowFadeInView>
        );
      } else {
        return null;
      }
    } else if (A1==1&&count==8&&global.ch1B==true) {
      if (this.state.fontsLoaded) {
        return (
          <SlowFadeInView style={styles.container}>
            <View style={styles.container0}>
              <View style={styles.rsIconContainer}>
                <FadeInView>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={this.pressDogi}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </FadeInView>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 5</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.pressDogi} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.A2Dialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <SlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </SlowFadeInView>
              </View>
            </View>
          </SlowFadeInView>
        );
      } else {
        return null;
      }
    }  else if (A1==1&&count==9&&global.ch1B==true) {
      if (this.state.fontsLoaded) {
        return (
          <SlowFadeInView style={styles.container}>
            <VerySlowFadeInView>
              <Modal 
              animationType = {"fade"}
              transparent ={true}
              visible = {modalVisible}
              statusBarTranslucent={true}
              onRequestClose = {()=> {this.setModalVisibile(!modalVisible)}}>
              <View style={{backgroundColor:'#00000090',flex:1,flexDirection:'column'}}>
                <View style={{flexDirection:'row',flex:2,alignItems:'center',}}>
                <View style={{flexDirection:'row'}}>
                <View style={{padding:10,opacity:0}}>
                  <Icon name="heart" size={50}/>
                </View>
                <TouchableOpacity activeOpacity={1} style={{backgroundColor:'#DADBCE', padding:10, borderRadius:20,borderColor:'black',flex:1,borderWidth:1}} onPress={()=>{this.setModalVisibile(!modalVisible)}}>
                <View style={{flexDirection:'column'}}>
                <FadeInView>
                  <FadeInView style={{flexDirection:'row'}}>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'black'}}>{count2==0 ? <Text>{this.state.modalDialogue[0]}</Text> : <FadeInView style={{flexDirection:'row',flex:1}}><Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'black',flex:1}}>{this.state.modalDialogue[1]}</Text></FadeInView>}</Text>
                  </FadeInView>
                </FadeInView>
                </View>
                <FastFadeInView>
                  <Text style={{fontSize:12,lineHeight:18}}>Tap here to continue..</Text>
                </FastFadeInView>
                </TouchableOpacity>
                </View>

                </View>

                <View style={{flex:8}}>

                </View>
              </View>                
              </Modal>
            </VerySlowFadeInView>

            <View style={styles.container0}>
              <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 5</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice1DONE} activeOpacity={1} touchSoundDisabled={true}>
                <View>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.A2Dialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <View key={index}>
                        <Lines say={content}/>
                      </View>
                    )
                  })
                } 
                </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <SlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </SlowFadeInView>
              </View>
            </View>
          </SlowFadeInView>
        );
      } else {
        return null;
      }
    } 
    
    // SYNPOSIS AND CHOICE2 (CH1A)
    else if (A2==0&&count<5&&global.ch1A==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineB} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView style={{flex:1}}>
                <Text style={{flex:1}}>
                  {/* SYNOPSIS OF CH1A */}
                  {count==0 ? 
                    <FadeInView style={{flex:1}}>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,flex:1,width:350}}>{this.state.BADialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.BADialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.BADialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==3 ? 
                    <FadeInView style={{flex:1}}>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,flex:1,width:350}}>{this.state.BADialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView style={{flex:1}}>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,flex:1,width:350}}>{this.state.BADialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                
                </Text>
                </FadeInView>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <SlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </SlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A2==0&&count>4&&count<7&&global.ch1A==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 10</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineB} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.BADialogue[5]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FIView key={index}>
                        <Lines say={content}/>
                      </FIView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                 </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <SlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </SlowFadeInView>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A2==0&&count==7&&global.ch1A==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 10</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.BADialogue[5]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FIView key={index}>
                        <Lines say={content}/>
                      </FIView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                 </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.prank}>
                    <Text style={{fontFamily:'RetroGaming'}}>PRANK</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.reconsider}>
                    <Text style={{fontFamily:'RetroGaming'}}>RECONSIDER</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } 

    // SYNPOSIS AND CHOICE2 (CH1B) SEGWAY TO BULLY CHOICE
    else if (A2==0&&count<5&&global.ch1B==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
              <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineB} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView style={{flex:1}}>
                <Text style={{flex:1}}>
                  {/* SYNOPSIS OF CH1A */}
                  {count==0 ? 
                    <FadeInView style={{flex:1}}>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,flex:1,width:350}}>{this.state.BBDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.BBDialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.BBDialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==3 ? 
                    <FadeInView style={{flex:1}}>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,flex:1,width:350}}>{this.state.BBDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView style={{flex:1}}>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,flex:1,width:350}}>{this.state.BBDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                
                </Text>
                </FadeInView>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <SlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </SlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A2==0&&count>4&&count<7&&global.ch1B==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 10</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineB} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.BADialogue[5]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FIView key={index}>
                        <Lines say={content}/>
                      </FIView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                 </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <SlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </SlowFadeInView>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A2==0&&count==7&&global.ch1B==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 10</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.BADialogue[5]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FIView key={index}>
                        <Lines say={content}/>
                      </FIView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                 </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.report}>
                    <Text style={{fontFamily:'RetroGaming'}}>REPORT</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.ignore}>
                    <Text style={{fontFamily:'RetroGaming'}}>IGNORE</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } 
    
    // PRANK CHOICE (CH1AA)
    else if (A2==1&&count>=0&&count<=5&&global.ch1AA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 10</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineB} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                  {count==0 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B1Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B1Dialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B1Dialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B1Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B1Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B1Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                </FadeInView>
                </TouchableOpacity>
                 </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A2==1&&count==6&&global.ch1AA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 10</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice2DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>
                  {count==6 ? 
                  <VerySlowFadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,flex:1,width:350}}>{this.state.B1Dialogue[6]}</Text>
                  </VerySlowFadeInView>
                  : null}
                </Text>
                </FadeInView>
                </TouchableOpacity>
                 </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // RECONSIDER CHOICE (CH1AB)
    else if (A2==1&&count>=0&&count<=1&&global.ch1AB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 10</Text>
                </View>
              </View>
              </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineB} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B2Dialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                }
                </FIView>
                </TouchableOpacity>
                 </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A2==1&&count==2&&global.ch1AB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 10</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice2DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,flex:1,width:350}}>{this.state.B2Dialogue[2]}</Text>
                  </FadeInView>
                  : null}
                </Text>
                </FadeInView>
                </TouchableOpacity>
                 </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // REPORT (CH1BA)
    else if (A2==1&&count>=0&&count<4&&global.ch1BA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 10</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineB} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                {count==0 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B3Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B3Dialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B3Dialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B3Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                </FadeInView>
                </TouchableOpacity>
                 </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A2==1&&count==4&&global.ch1BA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 10</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice2DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>
                {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B3Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                </Text>
                </FadeInView>
                </TouchableOpacity>
                 </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // IGNORE (CH1BB)
    else if (A2==1&&count>=0&&count<4&&global.ch1BB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 10</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineB} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                    {count==0 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B4Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                    {count==1 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B4Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                    {count==2 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B4Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                    {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B4Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                </FadeInView>
                </TouchableOpacity>
                 </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A2==1&&count==4&&global.ch1BB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 10</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice2DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                {count==4 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.B4Dialogue[count]}</Text>
                    </FIView> 
                    : null}
                </FadeInView>
                </TouchableOpacity>
                 </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // SYNOPSIS AND CHOICE3 (CH1AA) FRANK DEAD
    else if (A3==0&&count>=0&&count<4&&global.ch1AA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AA */}
                  {count==0 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CADialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CADialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CADialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CADialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CADialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CADialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A3==0&&count==4&&global.ch1AA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 15</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AA */}
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CADialogue[count]}</Text>
                    </FadeInView> 
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A3==0&&count>=5&&global.ch1AA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 15</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AA */}
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CADialogue[4]}</Text>
                    </FadeInView> 

                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CADialogue[count]}</Text>
                    </FadeInView> 

                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.makeConnections}>
                    <Text style={{fontFamily:'RetroGaming'}}>MAKE CONNECTIONS</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.study}>
                    <Text style={{fontFamily:'RetroGaming'}}>STUDY</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // SYNOPSIS AND CHOICE3 (CH1AB) RECONSIDER ROUTE FRANK IS ALIVE
    else if (A3==0&&count>=0&&count<4&&global.ch1AB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AA */}
                  {count==0 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CBDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CBDialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CBDialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CBDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CBDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CBDialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A3==0&&count==4&&global.ch1AB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 15</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AA */}
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CBDialogue[count]}</Text>
                    </FadeInView> 
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A3==0&&count>=5&&global.ch1AB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 15</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AA */}
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CBDialogue[4]}</Text>
                    </FadeInView> 

                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CBDialogue[count]}</Text>
                    </FadeInView> 

                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.makeConnections}>
                    <Text style={{fontFamily:'RetroGaming'}}>MAKE CONNECTIONS</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.study}>
                    <Text style={{fontFamily:'RetroGaming'}}>STUDY</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // SYNOPSIS AND CHOICE 3 (CH1BA) REPORT BULLIES
    else if (A3==0&&count>=0&&count<6&&global.ch1BA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AA */}
                  {count==0 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CCDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CCDialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CCDialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CCDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CCDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CCDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A3==0&&count==6&&global.ch1BA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 15</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AA */}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CCDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A3==0&&count>=7&&global.ch1BA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 15</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AA */}
                    <View>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CCDialogue[6]}</Text>
                    </View> 

                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CCDialogue[7]}</Text>
                    </FadeInView> 

                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.makeConnections}>
                    <Text style={{fontFamily:'RetroGaming'}}>MAKE CONNECTIONS</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.study}>
                    <Text style={{fontFamily:'RetroGaming'}}>STUDY</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // SYNOPSIS AND CHOICE 3 (CH1BA) IGNORE VICTIM
    else if (A3==0&&count>=0&&count<5&&global.ch1BB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AA */}
                  {count==0 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CDDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CDDialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CDDialogue[count]}</Text>
                  </FadeInView> : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CDDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CDDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CDDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A3==0&&count==5&&global.ch1BB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 15</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AA */}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CDDialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                                    {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A3==0&&count>=6&&global.ch1BB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 15</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AA */}
                    <View>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CDDialogue[5]}</Text>
                    </View> 

                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.CDDialogue[6]}</Text>
                    </FadeInView> 

                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.makeConnections}>
                    <Text style={{fontFamily:'RetroGaming'}}>MAKE CONNECTIONS</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.study}>
                    <Text style={{fontFamily:'RetroGaming'}}>STUDY</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // MAKE CONNECTIONS CHOICE AND SYNOPSIS (CH1AAA) FRANK DEAD PARTY
    else if (A3==1&&count>=0&&count<11&&global.ch1AAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==8 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==9 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==10 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A3==1&&count==11&&global.ch1AAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice3DONE} activeOpacity={1} touchSoundDisabled={true}>
                <View>
                  {/* SYNOPSIS OF CH1AAA */}
                    <View>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[10]}</Text>
                    </View> 
                  {count==11 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C1Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </View>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    // CHOICE 4 (CH1AAA)
    } else if (A4==0&&count>=0&&count<5&&global.ch1AAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 20</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DADialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==0&&count==5&&global.ch1AAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DADialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.ignoreCall}>
                    <Text style={{fontFamily:'RetroGaming'}}>IGNORE</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.answerCall}>
                    <Text style={{fontFamily:'RetroGaming'}}>ANSWER</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } 

    // MAKE CONNECTIONS CHOICE AND SYNOPSIS (CH1ABA) FRANK DEAD PARTY
    else if (A3==1&&count>=0&&count<11&&global.ch1ABA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==8 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==9 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==10 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A3==1&&count==11&&global.ch1ABA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice3DONE} activeOpacity={1} touchSoundDisabled={true}>
                <View>
                  {/* SYNOPSIS OF CH1AAA */}
                    <View>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[10]}</Text>
                    </View> 
                  {count==11 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C3Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </View>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    // CHOICE 4 (CH1AAA)
    } else if (A4==0&&count>=0&&count<5&&global.ch1ABA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 20</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DCDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==0&&count==5&&global.ch1ABA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DCDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.ignoreCall}>
                    <Text style={{fontFamily:'RetroGaming'}}>IGNORE</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.answerCall}>
                    <Text style={{fontFamily:'RetroGaming'}}>ANSWER</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } 

    // MAKE CONNECTIONS CHOICE AND SYNOPSIS (CH1BAA)
    else if (A3==1&&count>=0&&count<11&&global.ch1BAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==8 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==9 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==10 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A3==1&&count==11&&global.ch1BAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice3DONE} activeOpacity={1} touchSoundDisabled={true}>
                <View>
                  {/* SYNOPSIS OF CH1AAA */}
                    <View>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[10]}</Text>
                    </View> 
                  {count==11 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C5Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </View>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    // CHOICE 4 (CH1AAA)
    } else if (A4==0&&count>=0&&count<5&&global.ch1BAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 20</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DEDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==0&&count==5&&global.ch1BAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DEDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.ignoreCall}>
                    <Text style={{fontFamily:'RetroGaming'}}>IGNORE</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.answerCall}>
                    <Text style={{fontFamily:'RetroGaming'}}>ANSWER</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } 

    // MAKE CONNECTIONS CHOICE AND SYNOPSIS (CH1BBA)
    else if (A3==1&&count>=0&&count<11&&global.ch1BBA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==8 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==9 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==10 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A3==1&&count==11&&global.ch1BBA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice3DONE} activeOpacity={1} touchSoundDisabled={true}>
                <View>
                  {/* SYNOPSIS OF CH1AAA */}
                    <View>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[10]}</Text>
                    </View> 
                  {count==11 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C7Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </View>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    // CHOICE 4 (CH1AAA)
    } else if (A4==0&&count>=0&&count<5&&global.ch1BBA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 20</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DGDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==0&&count==5&&global.ch1BBA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DGDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.ignoreCall}>
                    <Text style={{fontFamily:'RetroGaming'}}>IGNORE</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.answerCall}>
                    <Text style={{fontFamily:'RetroGaming'}}>ANSWER</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } 

    // STUDY CHOICE AND SYNOPSIS (CH1AAB) FRANK DEAD
    else if (A3==1&&count>=0&&count<11&&global.ch1AAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==8 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==9 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==10 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A3==1&&count==11&&global.ch1AAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice3DONE} activeOpacity={1} touchSoundDisabled={true}>
                <View>
                  {/* SYNOPSIS OF CH1AAA */}
                    <View>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[10]}</Text>
                    </View> 
                  {count==11 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C2Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </View>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    // CHOICE 4 (CH1AAA)
    } // STUDY CHOICE 4 (CH1AAB)
    else if (A4==0&&count>=0&&count<5&&global.ch1AAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 20</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DBDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==0&&count==5&&global.ch1AAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DBDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.goInterview}>
                    <Text style={{fontFamily:'RetroGaming'}}>GO TO INTERIVEW</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.goMom}>
                    <Text style={{fontFamily:'RetroGaming'}}>GO TO MOM</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } 

    // STUDY CHOICE AND SYNOPSIS (CH1ABB) FRANK ALIVE
    else if (A3==1&&count>=0&&count<11&&global.ch1ABB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==8 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==9 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==10 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A3==1&&count==11&&global.ch1ABB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice3DONE} activeOpacity={1} touchSoundDisabled={true}>
                <View>
                  {/* SYNOPSIS OF CH1AAA */}
                    <View>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[10]}</Text>
                    </View> 
                  {count==11 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C4Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </View>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    // CHOICE 4 (CH1AAA)
    } // STUDY CHOICE 4 (CH1ABB)
    else if (A4==0&&count>=0&&count<5&&global.ch1ABB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 20</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DDDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==0&&count==5&&global.ch1ABB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DDDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.goInterview}>
                    <Text style={{fontFamily:'RetroGaming'}}>GO TO INTERIVEW</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.goMom}>
                    <Text style={{fontFamily:'RetroGaming'}}>GO TO MOM</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } 

    // STUDY CHOICE AND SYNOPSIS (CH1BAB) 
    else if (A3==1&&count>=0&&count<11&&global.ch1BAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==8 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==9 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==10 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A3==1&&count==11&&global.ch1BAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice3DONE} activeOpacity={1} touchSoundDisabled={true}>
                <View>
                  {/* SYNOPSIS OF CH1AAA */}
                    <View>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[10]}</Text>
                    </View> 
                  {count==11 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C6Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </View>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    // CHOICE 4 (CH1AAA)
    } // STUDY CHOICE 4 (CH1BAB)
    else if (A4==0&&count>=0&&count<5&&global.ch1BAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 20</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DFDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==0&&count==5&&global.ch1BAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DFDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.goInterview}>
                    <Text style={{fontFamily:'RetroGaming'}}>GO TO INTERIVEW</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.goMom}>
                    <Text style={{fontFamily:'RetroGaming'}}>GO TO MOM</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } 

    // STUDY CHOICE AND SYNOPSIS (CH1BBB)
    else if (A3==1&&count>=0&&count<11&&global.ch1BBB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineC} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==8 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==9 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==10 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    } else if (A3==1&&count==11&&global.ch1BBB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FIView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}></Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice3DONE} activeOpacity={1} touchSoundDisabled={true}>
                <View>
                  {/* SYNOPSIS OF CH1AAA */}
                    <View>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[10]}</Text>
                    </View> 
                  {count==11 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.C8Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </View>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FIView>
        );
      } else {
        return null;
      }
    // CHOICE 4 (CH1AAA)
    } // STUDY CHOICE 4 (CH1BBB)
    else if (A4==0&&count>=0&&count<5&&global.ch1BBB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <VerySlowFadeInView>
                  <Text style={styles.ageText}>AGE 20</Text>
                </VerySlowFadeInView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DHDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==0&&count==5&&global.ch1BBB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <FadeInView>
                <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.DHDialogue[0]}</Text>
                {
                  this.state.empty.map((content,index) => {
                    return (
                      <FadeInView key={index}>
                        <Lines say={content}/>
                      </FadeInView>
                    )
                  })
                } 
                </FadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.goInterview}>
                    <Text style={{fontFamily:'RetroGaming'}}>GO TO INTERIVEW</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choicebox} onPress={this.goMom}>
                    <Text style={{fontFamily:'RetroGaming'}}>GO TO MOM</Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } 

    // IGNORE CALL CHOICE (CH1AAAA)
    else if (A4==1&&count>=0&&count<5&&global.ch1AAAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D1Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D1Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D1Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D1Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D1Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D1Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1AAAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D1Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    //IGNORE CALL CHOICE (CH1ABAA)
    else if (A4==1&&count>=0&&count<5&&global.ch1ABAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D5Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D5Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D5Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D5Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D5Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D5Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1ABAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D5Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    //IGNORE CALL CHOICE (CH1BAAA)
    else if (A4==1&&count>=0&&count<5&&global.ch1BAAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D9Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D9Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D9Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D9Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D9Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D9Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1BAAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D9Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    //IGNORE CALL CHOICE (CH1BBAA)
    else if (A4==1&&count>=0&&count<5&&global.ch1BBAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D13Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D13Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D13Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D13Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D13Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D13Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1BBAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D13Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // ANSWER CALL CHOICE (CH1AAAB)
    else if (A4==1&&count>=0&&count<5&&global.ch1AAAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D2Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D2Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D2Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D2Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D2Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D2Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1AAAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D2Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // ANSWER CALL CHOICE (CH1ABAB)
    else if (A4==1&&count>=0&&count<5&&global.ch1ABAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D6Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D6Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D6Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D6Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D6Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D6Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1ABAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D6Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // ANSWER CALL CHOICE (CH1BAAB)
    else if (A4==1&&count>=0&&count<5&&global.ch1BAAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D10Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D10Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D10Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D10Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D10Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D10Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1BAAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D10Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // ANSWER CALL CHOICE (CH1BBAB)
    else if (A4==1&&count>=0&&count<5&&global.ch1BBAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D14Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D14Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D14Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D14Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D14Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D14Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1BBAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D14Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // GO TO INTERVIEW (CH1AABA)
    else if (A4==1&&count>=0&&count<5&&global.ch1AABA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D3Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D3Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D3Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D3Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D3Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D3Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1AABA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D3Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // GO TO INTERVIEW (CH1ABBA)
    else if (A4==1&&count>=0&&count<5&&global.ch1ABBA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D7Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D7Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D7Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D7Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D7Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D7Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1ABBA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D7Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // GO TO INTERVIEW (CH1BABA)
    else if (A4==1&&count>=0&&count<5&&global.ch1BABA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D11Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D11Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D11Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D11Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D11Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D11Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1BABA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D11Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // GO TO INTERVIEW (CH1BBBA)
    else if (A4==1&&count>=0&&count<5&&global.ch1BBBA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D15Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D15Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D15Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D15Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D15Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D15Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1BBBA==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D15Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // GO TO MOM (CH1AABB)
    else if (A4==1&&count>=0&&count<5&&global.ch1AABB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1AABB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // GO TO MOM (CH1ABBB)
    else if (A4==1&&count>=0&&count<5&&global.ch1ABBB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1ABBB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D8Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // GO TO MOM (CH1BABB)
    else if (A4==1&&count>=0&&count<5&&global.ch1BABB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D12Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D12Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D12Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D12Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D12Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D12Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1BABB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D12Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // GO TO MOM (CH1BBBB)
    else if (A4==1&&count>=0&&count<5&&global.ch1BBBB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D16Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D16Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D16Dialogue[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D16Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D16Dialogue[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D16Dialogue[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    } else if (A4==1&&count==5&&global.ch1BBBB==true) {
      if (this.state.fontsLoaded) {
        return (
          <FadeInView style={styles.container}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#CED49F" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <View>
                  <Text style={styles.ageText}>AGE 20</Text>
                </View>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainer}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.choice4DONE} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==5 ? 
                    <FIView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18}}>{this.state.D16Dialogue[count]}</Text>
                    </FIView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2}>
              <View style={styles.choicecontainer}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainer}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </FadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDINGS

    // ENDING CH1AAAA IGNORE CALL FAMILY DISOWN
    else if (end==1&&count>=0&&count<7&&global.ch1AAAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}1 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAA[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAA[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1AAAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}1 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAA[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDING CH1AAAB ANSWER CALL FAMILY RESPECC
    else if (end==1&&count>=0&&count<7&&global.ch1AAAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}2 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAB[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAB[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1AAAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}2 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AAAB[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDING CH1AABA SCHOOL INTERVIEW FAMILY UPSET WITH YOU
    else if (end==1&&count>=0&&count<7&&global.ch1AABA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}3 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABA[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABA[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1AABA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}3 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABA[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDING CH1AABB MOM FAMILY RESPECC PUBLIC SCHOOL
    else if (end==1&count>=0&&count<7&&global.ch1AABB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}4 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABB[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABB[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1AABB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}4 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1AABB[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDING CH1ABAA IGNORE CALL
    else if (end==1&count>=0&&count<7&&global.ch1ABAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}5 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAA[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAA[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1ABAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}5 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAA[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDING CH1ABAB ANSWER CALL
    else if (end==1&count>=0&&count<7&&global.ch1ABAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}6 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAB[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAB[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1ABAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}6 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAB[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDING CH1ABBA 
    else if (end==1&&count>=0&&count<7&&global.ch1ABBA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}7 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBA[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBA[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1ABBA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}7 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBA[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }
    
    // ENDING CH1ABBB 
    else if (end==1&count>=0&&count<7&&global.ch1ABBB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}8 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBB[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBB[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1ABBB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}8 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABBB[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDING CH1BAAA 
    else if (end==1&count>=0&&count<7&&global.ch1BAAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}9 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAA[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAA[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1BAAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}9 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAA[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDING CH1BAAB 
    else if (end==1&count>=0&&count<7&&global.ch1BAAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}10 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAB[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAB[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1BAAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}10 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BAAB[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDING CH1BABA
    else if (end==1&&count>=0&&count<7&&global.ch1BABA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}11 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABA[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABA[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1BABA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}11 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABA[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }
    
    // ENDING CH1BABB
    else if (end==1&count>=0&&count<7&&global.ch1BABB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}12 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABB[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABB[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1BABB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}12 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BABB[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDING CH1BBAA
    else if (end==1&count>=0&&count<7&&global.ch1BBAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}13 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAA[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAA[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1BBAA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}13 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAA[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDING CH1BBAB
    else if (end==1&count>=0&&count<7&&global.ch1BBAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}14 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAB[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBAB[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1BBAB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}14 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1ABAB[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

    // ENDING CH1BBBA
    else if (end==1&&count>=0&&count<7&&global.ch1BBBA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}15 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBA[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBA[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBA[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBA[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1BBBA==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}15 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBA[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }
    
    // ENDING CH1BBBB
    else if (end==1&count>=0&&count<7&&global.ch1BBBB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}16 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} onPress={this.nextLineD} activeOpacity={1} touchSoundDisabled={true}>
                <FIView>
                  {/* SYNOPSIS OF CH1AAA */}
                  {count==0 ? 
                  <FadeInView>
                   <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==1 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==2 ? 
                  <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBB[count]}</Text>
                  </FadeInView> 
                    : null}
                  {count==3 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==4 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBB[count]}</Text>
                    </FadeInView> 
                    : null}
                  {count==5 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBB[count]}</Text>
                    </FadeInView> 
                  : null}
                  {count==6 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBB[count]}</Text>
                    </FadeInView> 
                  : null}
                </FIView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <View style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }     else if (count==7&&global.ch1BBBB==true) {
      if (this.state.fontsLoaded) {
        return (
          <VerySlowFadeInView style={styles.containerEND}>
            <View style={styles.container0}>
            <View style={styles.rsIconContainer}>
                <View>
                  <TouchableOpacity style={{padding:10,opacity:5}} onPress={() => this.props.navigation.navigate('RS')}>
                    <Icon name="heart" color="#5B6582" size={50}/>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.ageContainer}>
                <FIView>
                  <Text style={styles.ageTextEND}>END{'\n'}16 of 16</Text>
                </FIView>
              </View>
            </View>

            <View style={styles.container1}>
                {/* PICTURES WILL GO HERE */}
               <View style={styles.piccontainerEND}>
  
               </View>
                {/* TEXT WILL GO HERE */}
                <View style={styles.textcontainer2END}>
                <TouchableOpacity style={styles.continuebutton} activeOpacity={1} touchSoundDisabled={true}>
                <VerySlowFadeInView>
                  {count==7 ? 
                    <FadeInView>
                    <Text style={{fontFamily:'RetroGaming',fontSize:12,lineHeight:18,color:'white'}}>{this.state.endingCH1BBBB[count]}</Text>
                    </FadeInView> 
                  : null}
                </VerySlowFadeInView>
                </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container2END}>
              <View style={styles.choicecontainerEND}>
                {/* CHOICES WILL GO HERE */}
                <VerySlowFadeInView style={styles.actioncontainerEND}>
                {/* CHOICE1 */}
                  <TouchableOpacity style={styles.choiceboxEND} onPress={this.retryGame}>
                    <Text style={{fontFamily:'RetroGaming'}}>RETRY</Text>
                  </TouchableOpacity>
                  <View style={styles.space}>
                  </View>
                {/* CHOICE2 */}
                  <TouchableOpacity style={styles.choiceboxinvis}>
                    <Text style={{fontFamily:'RetroGaming'}}></Text>
                  </TouchableOpacity>
                </VerySlowFadeInView>
              </View>
            </View>
          </VerySlowFadeInView>
        );
      } else {
        return null;
      }
    }

  }
}

const styles = StyleSheet.create ({
  container: {
    flexDirection: 'column',
    flex:1,
    padding:1,
    backgroundColor:'#52543F'
  },
  container0: {
    flex:0.8,
    flexDirection:'row'
  },
  rsIconContainer: {
    flex:2,
    padding:5,
    flexDirection:'column-reverse'
  },
  ageContainer: {
    flex:1,
    padding:10,
    flexDirection:'column-reverse'
  },
  ageText: {
    fontFamily:'RetroGaming',
    fontSize:25,
    color:'#DADBCE'
  },
  container1: {
    flex:5,
    borderColor: 'black',
    borderWidth:1,
    backgroundColor: 'white'
  },
  piccontainer: {
    flex:1,
    backgroundColor:'#121212'
  },
  textcontainer:{
    flex:1.5,
    backgroundColor:'black',
    opacity:0.9,
    alignItems:'center',
    justifyContent:'center'
  },
  textcontainer2:{
    flex:1.5,
    backgroundColor:'#DADBCE'
  },
  container2: {
    flex:1,
    justifyContent:'space-evenly',
    backgroundColor: '#DADBCE'
  },
  actioncontainer:{
    flex:1,
    padding:20,
    flexDirection:'row',
    alignContent: 'center',
    backgroundColor:'#52543F'
  },
  choicecontainer:{
    flex:1,
    flexDirection:'row',
    alignContent: 'center',
    backgroundColor:'#52543F'
  },
  choicebox: {
    flex:1,
    borderRadius:20,
    borderColor: 'black',
    borderWidth: 0.5,
    justifyContent:'center',
    alignItems:'center',
    textAlign:'center',
    backgroundColor:'#9A9E77'
  },
  choiceboxinvis: {
    flex:1,
    borderRadius:20,
    borderColor: 'black',
    justifyContent:'center',
    alignItems:'center',
  },
  space: {
    flex:0.5
  },
  continuebutton: {
    flex:1,
    flexDirection: 'column',
    padding: 30,
  },
  //END COLORS
  containerEND: {
    flexDirection: 'column',
    flex:1,
    padding:1,
    backgroundColor:'#26272B'
  },
  ageTextEND: {
    fontFamily:'RetroGaming',
    fontSize:25,
    color:'#5B6582'
  },
  textcontainer2END:{
    flex:1.5,
    backgroundColor:'#43454D'
  },
  container2END: {
    flex:1,
    justifyContent:'space-evenly',
    backgroundColor: 'black'
  },
  actioncontainerEND:{
    flex:1,
    padding:20,
    flexDirection:'row',
    alignContent: 'center',
    backgroundColor:'#26272B'
  },
  choicecontainerEND:{
    flex:1,
    flexDirection:'row',
    alignContent: 'center',
    backgroundColor:'#26272B'
  },
  piccontainerEND: {
    flex:1,
    backgroundColor:'#131313'
  },
  choiceboxEND: {
    flex:1,
    borderRadius:20,
    borderColor: 'black',
    borderWidth: 0.5,
    justifyContent:'center',
    alignItems:'center',
    textAlign:'center',
    backgroundColor:'#5B6582'
  }
})
