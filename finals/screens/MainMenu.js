import React from 'react';
import { TouchableOpacity, View, StyleSheet, StatusBar, Image, Dimensions} from 'react-native';


export default class MainMenu extends React.Component{


    componentDidMount() {
        StatusBar.setHidden(true);
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.touch} onPress={() => this.props.navigation.replace('Tutorial')} activeOpacity={1}>
                    <Image style={{ flex:1, width: undefined, height: undefined, aspectRatio: 1 , resizeMode:'contain'}} source={require('../assets/mainmenu1.gif')}/>
                </TouchableOpacity>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#52543F',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center'
    },
    text: {
        fontSize: 20,
        color: 'black',
    },
    touch: {
        flex: 1,
        borderWidth:20,
        borderColor:'black'
    }
})