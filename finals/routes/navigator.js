import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import Tutorial from "../screens/tutorial";
import Chapter1 from "../screens/Chapter1";
import MainMenu from "../screens/MainMenu";
import Relationships from "../screens/relationships";


const screens = {
    Home: {
        screen: MainMenu,
        
    },
    RS: {
        screen: Relationships
    },
    Tutorial: {
        screen: Tutorial
    },
    C1: {
        screen: Chapter1
    }

}

const Stack = createStackNavigator(screens, {
    defaultNavigationOptions: {
        headerShown: 0,
    }
});


export default createAppContainer(Stack);