import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {KeyboardAvoidingView, Platform, StyleSheet, Text, View, TextInput, TouchableOpacity, Button, Keyboard } from 'react-native';

export default function App ( { navigation }) {
  const pressHandler = () => {
    navigation.navigate('Navigation')
  }
  const [task, setTask] = useState();
  const [taskItems, setTaskItems] = useState([]);
  
  // ADDING TASK
  const addTask = () => {
    Keyboard.dismiss();
    setTaskItems([task,...taskItems])
    setTask(null);  
  }
  //DELETE TASK
    const completeTask = (index) => {
      let itemsCopy = [...taskItems];
      itemsCopy.splice(index, 1);
      setTaskItems(itemsCopy);
    }
  // TASK BOX APPEARANCE
    const Task = (data) => {

      return (
          <View style={styles.item}>
              <View style={styles.itemLeft}>
                  <View style={styles.bullet}>
                  </View>
                  <Text style={styles.itemText}>{data.text}</Text>
              </View>
          </View>
      )
  }  
  return (
    <View style = {styles.container}>
      {/* ICON NAVIGATOR */}
       <TouchableOpacity style={styles.button} onPress={pressHandler} >
        <View style={styles.iconchar}>
        <Icon name="trophy" size={30} color="#edd74a" />
        </View>
      </TouchableOpacity>

      {/* TEXTBOX */}
      
      <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.txtBox}
      >
        <TextInput style={styles.input} placeholder= {'Write a task'} value={task} onChangeText={text => setTask(text)} />
        
      </KeyboardAvoidingView>
      {/* BUTTON */}
      <View style = {styles.butcont}>
        <Button title = "Add task" onPress={() =>addTask()} color = 'black'/>
      </View>

      {/* TO DO LIST */}
      <View style={styles.tasksWrapper}>
        <Text style={styles.sectionTitle}>To Do List:</Text>
        
        {/* TASK ITEMS */}
        <View style={styles.items}> 
        
          {
            taskItems.map((item, index) => {
              return (
                <TouchableOpacity key={index} >
                <TouchableOpacity key={index} onPress={() => completeTask(index)}>
                <View style={styles.checkbox}>
                <Text style={styles.check}>✓</Text> 
                </View>
                </TouchableOpacity>
                <Task text={item} />
                </TouchableOpacity>
              )
            })
          }
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed"
  },
  //ICON
  button: {
    marginLeft: 15,
    top:5,
    height: 40,
    width: 40,
    borderRadius: 5,
    borderColor:"gray",
    borderWidth: 1,
    backgroundColor: "white"
  },
  iconchar: {
    margin:2,
    borderColor: 'black',
    borderRadius: 5
  },
  // TEXTBOX AND TITLE
  tasksWrapper:{
    paddingTop: 90,
    paddingHorizontal:20,
  },
  sectionTitle:{
    fontSize: 24,
    fontWeight: 'bold'
  },
  items:{
    marginTop: 25,
  },
  txtBox: {
    position: 'absolute',
    top: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  input: {
    paddingVertical: 15,
    paddingHorizontal: 20,
    backgroundColor: '#FFF',
    borderRadius: 20,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    width: 250,
  },
  butcont: {
    top: 80,
    alignItems: "center",
    justifyContent: "center",
  },
  // TASK CONTAINER
  item: {
    backgroundColor: '#FFF',
    width: 325,
    padding: 15,
    borderRadius:10,
    left: 50,
    bottom: 35,
  },
  itemLeft: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
    },
  checkbox: {
    width: 28,
    height: 28,
    backgroundColor: 'black',
    opacity: 0.4,
    borderRadius: 5,
    marginRight: 15,
    },
  bullet: {
    width: 10,
    height: 10,
    backgroundColor: 'black',
    opacity: 0.4,
    borderRadius: 20,
    marginRight: 15,
  },
  itemText: {
    maxWidth: '80%',
    },
  check: {
    fontSize: 20,
    marginLeft: 7,
    fontWeight: 'bold',
    }
});
