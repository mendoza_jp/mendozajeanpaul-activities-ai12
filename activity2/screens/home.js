import React from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';


export default function Home({ navigation }) {
    
    return (
        <View style={styles.container}>
            <Text style={styles.randomtext}>Completed Tasks Go here:</Text>
        </View>
    )
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    randomtext: {
        marginTop: 12,
        fontSize: 21,
        left:10,
    },
    content: {
        
    }
});