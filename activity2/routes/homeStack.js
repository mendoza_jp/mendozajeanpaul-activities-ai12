import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import ToDo from "../screens/ToDo"
import Home from "../screens/home.js";

const screens = {
    ToDoApp: {
        screen: ToDo
    },
    Navigation: {
        screen: Home
    }
    
}

const HomeStack = createStackNavigator(screens); 

export default createAppContainer(HomeStack);