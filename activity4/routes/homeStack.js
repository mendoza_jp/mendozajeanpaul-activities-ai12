import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import Home from "../screens/home.js";
import Screen2 from "../screens/screen2";
import Screen3 from "../screens/screen3";
import Screen4 from "../screens/screen4";
import Screen5 from "../screens/screen5";
import Screen6 from "../screens/screen6";

const screens = {
    Navigation: {
        screen: Home
    },
    Screen2: {
        screen: Screen2
    },
    Screen3: {
        screen: Screen3
    },
    Screen4: {
        screen: Screen4
    },
    Screen5: {
        screen: Screen5
    },
    Screen6: {
        screen: Screen6
    }
    
}

const HomeStack = createStackNavigator(screens, {
    defaultNavigationOptions: {
        headerShown: 0,
    }
}); 

export default createAppContainer(HomeStack);