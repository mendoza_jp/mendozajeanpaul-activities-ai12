import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Ionicon from 'react-native-vector-icons/Ionicons';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';


export default function Home({ navigation }) {
    const pressHandler = () => {
        navigation.navigate('Screen5')
      }
    const pressBack = () => {
        navigation.goBack();
    }

    return (
        <View style={styles.container}>
            {/* BACK ICON */}
            
            <TouchableOpacity style={{left: 20,top:50}} onPress={pressBack}>
                    <Icon name="arrow-back-ios" size={30} color="black" />
            </TouchableOpacity>
            <View style={styles.circle}>
                <View>
                    <Image source={require('../assets/screen3_img.png')}
                    style={{width:400,height:300}}
                    />
                </View>
            </View>
            
            <View style={styles.chatboxc}>
                <Ionicon name="chatbox" size={300} color="white" />
                <Text style={styles.title}>It is challenging</Text>
                <Text style={styles.desc}>Some tasks are difficult, which makes it quite satisfying when it is done. </Text>
            </View>
            
            {/* CONTINUE BUTTON */}
            <View>
                <TouchableOpacity style={styles.continue} onPress={pressHandler} >
                <Text style={styles.cont}>CONTINUE</Text>
                </TouchableOpacity>
            </View>
        </View>
        
    )
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: '#B1D0E0',
    },
    title: {
        fontSize: 21,
        bottom: 265,
        left: 40,
        fontWeight: 'bold'
    },
    desc: {
        opacity: 0.8,
        fontSize: 16,
        bottom: 265,
        left: 40,
        width: 230
    },
    // CONTINUE BUTTON
    continue: {
        bottom: 140,
        left: 185,
        height:50,
        width:125,
        backgroundColor: '#231930',
        borderColor: 'black',
        borderWidth:1,
        borderRadius: 35,
    },
    cont: {
        alignItems: 'center',
        flex: 1,
        top: 13,
        marginLeft: 27,
        fontWeight:'bold',
        color: 'white'
    },
    // CIRCLE
    circle: {
        backgroundColor: 'white',
        height: 300,
        width: 300,
        borderRadius:200,
        top: 40,
        left: 20,
    },
    // CHATBOX
    chatboxc: {
        top: 20,
        left: 40
    }
});