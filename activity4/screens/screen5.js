import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Fonta from 'react-native-vector-icons/FontAwesome5';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';


export default function Home({ navigation }) {
    const pressHandler = () => {
        navigation.navigate('Screen6')
      }
    const pressBack = () => {
        navigation.goBack();
    }

    return (
        <View style={styles.container}>
            {/* BACK ICON */}
            
            <TouchableOpacity style={{left: 20,top:50}} onPress={pressBack}>
                    <Icon name="arrow-back-ios" size={30} color="black" />
            </TouchableOpacity>
            <View style={styles.boxxx}>
                <View>
                    <Image source={require('../assets/screen4_img.png')}
                    style={{width:400,height:300}}
                    />
                </View>
            </View>

            <View style={styles.words}>
                <Text style={styles.title}>It is fun</Text>
                <Text style={styles.desc}>Coding and troubleshooting is fun, but the people you meet on the way to expertise make the experience more enjoyable.</Text>
            </View>
            
            <View style={styles.line}>
                <Fonta name="wave-square" size={150} color="black" />
            </View>
            
            {/* CONTINUE BUTTON */}
            <View>
                <TouchableOpacity style={styles.continue} onPress={pressHandler} >
                <Text style={styles.cont}>CONTINUE</Text>
                </TouchableOpacity>
            </View>
        </View>
        
    )
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: '#ACB992',
    },
    words: {
        alignContent: 'center',
        alignItems: 'center',
        top: 120,
        right: 20
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold'
    },
    desc: {
        opacity: 0.8,
        fontSize: 16,
        width: 250,
        left:85
    },
    // CONTINUE BUTTON
    continue: {
        top: 20,
        left: 240,
        height:50,
        width:125,
        backgroundColor: '#231930',
        borderColor: 'black',
        borderWidth:1,
        borderRadius: 35,
    },
    cont: {
        alignItems: 'center',
        flex: 1,
        top: 13,
        marginLeft: 27,
        fontWeight:'bold',
        color: 'white'
    },
    // BOXXX
    boxxx: {
        backgroundColor: '#FFE',
        borderRadius: 20,
        borderColor: 'black',
        borderWidth: 3,
        height: 220,
        width: 350,
        top: 50,
        left: 20,
    },
    // DESCRIPTION
    line: {
        top: 125,
        left: 40
    }
});