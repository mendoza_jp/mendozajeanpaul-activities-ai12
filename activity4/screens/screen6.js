import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Ent from 'react-native-vector-icons/Entypo';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';


export default function Home({ navigation }) {
    const pressHandler = () => {
        navigation.navigate('Navigation')
      }
    const pressBack = () => {
        navigation.goBack();
    }

    return (
        <View style={styles.container}>
            {/* BACK ICON */}
            
            <TouchableOpacity style={{left: 20,top:50}} onPress={pressBack}>
                    <Icon name="arrow-back-ios" size={30} color="black" />
            </TouchableOpacity>
            <View style={styles.shadow}>
                <View style={styles.imgg}>
                    <Image source={require('../assets/screen5_img.png')}
                    style={{width:275,height:200}}
                    />
                 </View>
            </View>
            
            <View style={styles.words}>
                <Text style={styles.title}>It is my dream</Text>
                <Text style={styles.desc}>Ever since I was a kid, I was always interested with this course. And now, I'm living the dream.</Text>
            </View>
            {/* HOME BUTTON */}
            <View>
                <TouchableOpacity style={styles.continue} onPress={pressHandler} >
                    <View style={{top:7,left:10}}>
                        <Ent name="home" size={30} color= "white" />
                    </View>
                    <Text style={styles.cont}>Home</Text>
                </TouchableOpacity>
            </View>
        </View>
        
    )
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    words: {
        alignContent: 'center',
        alignItems: 'center',
        left: 60,
        top: 200,
        width: 300,
    },
    title: {
        fontSize:30,
        fontWeight: 'bold',
    },
    desc: {
        color: '#3b3b3b',
        fontSize:16,
        justifyContent: 'space-evenly',
        width: 260,
    },
    // IMAGE
    imgg: {
        bottom: 125,
    },
    shadow: {
        left: 60,
        top: 200,
        backgroundColor: 'gray',
        height:80,
        width: 300,
        borderRadius: 500,
    },
    // CONTINUE BUTTON
    continue: {
        top: 300,
        left: 150,
        height:50,
        width:125,
        backgroundColor: '#231930',
        borderColor: 'black',
        borderWidth:1,
        borderRadius: 35,
    },
    cont: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
        bottom: 18,
        left: 50,
    },
    // BOXXX
    boxxx: {
        backgroundColor: '#FFE',
        borderRadius: 20,
        borderColor: 'black',
        borderWidth: 3,
        height: 220,
        width: 350,
        top: 50,
        left: 20,
    },
});