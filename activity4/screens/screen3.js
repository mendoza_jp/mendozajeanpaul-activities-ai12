import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';


export default function Home({ navigation }) {
    const pressHandler = () => {
        navigation.navigate('Screen4')
      }
    const pressBack = () => {
        navigation.goBack();
    }

    return (
        <View style={styles.container}>
            {/* BACK ICON */}
            <TouchableOpacity style={{left: 20,top:50}} onPress={pressBack}>
                    <Icon name="arrow-back-ios" size={30} color="black" />
            </TouchableOpacity>
            
            <View style={styles.imgg}>
                <Image source={require('../assets/screen2_img.png')}
                    style={{width:400,height:250,left: 5}}
                />
            </View>

            {/* WORDS */}
            <Text style={styles.title}>It is interesting</Text>
            <Text style={styles.desc}>The whole concept of IT piques my {'\n\t'} interest and it doesn't bore me.</Text>

            <View>
            {/* CONTINUE BUTTON */}
            <View>
                <TouchableOpacity style={styles.continue} onPress={pressHandler} >
                <Text style={styles.cont}>CONTINUE</Text>
                </TouchableOpacity>
            </View>
            </View>
            
        </View>
        
    )
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: '#FFE',
    },
    continue: {
        marginLeft: 30,
        height:20,
        width:150,
        color: '#FFF',
        borderColor: 'black',
        borderWidth:1,

    },
    title: {
        fontSize: 25,
        left:115,
        fontWeight:'bold',
        top: 40
    },
    desc: {
        fontSize: 16,
        left:80,
        top: 40,
        color: 'black',
        width: 300,
        borderRadius: 5
    },
    continue: {
        top:175,
        marginLeft: 145,
        height:50,
        width:125,
        backgroundColor: '#231930',
        borderColor: 'black',
        borderWidth:1,
        borderRadius: 35,
    },
    cont: {
        alignItems: 'center',
        flex: 1,
        top: 13,
        marginLeft: 27,
        fontWeight:'bold',
        color: 'white'
    },
    imgg: {
        top:50
    }
});