import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';


export default function Home({ navigation }) {
    const pressHandler = () => {
        navigation.navigate('Screen2')
      }

    return (
        //CLOUD
        <View style={styles.container}>
            <View style={styles.cloud}>
            <Icon name="cloud" size={600} color="#FBDFC8" />
            </View>
            <View style={styles.contrando}>
                <Text style={styles.title}>Reasons Why I Chose the {'\n\t\t\t\t\t\t\t\t'} IT Course</Text>
                <Image source={require('../assets/home_img0.png')}
                    style={{width:250,height:250,right:10}}
                />
            {/* CONTINUE BUTTON */}
            <View>
                <TouchableOpacity style={styles.continue} onPress={pressHandler} >
                <Text style={styles.cont}>CONTINUE</Text>
                </TouchableOpacity>
            </View>
            </View>
            
        </View>
        
    )
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    // BUTTON
    continue: {
        top:175,
        marginLeft: 150,
        height:50,
        width:125,
        backgroundColor: '#231930',
        borderColor: 'black',
        borderWidth:1,
        borderRadius: 35,
    },
    cont: {
        alignItems: 'center',
        flex: 1,
        top: 13,
        marginLeft: 27,
        fontWeight:'bold',
        color: 'white'
    },
    //CONTENT
    title: {
        fontSize: 25,
        right: 10,
        fontWeight: 'bold',
        alignItems:'center',
        alignContent: 'center',
        
    },
    //title & pic container
    contrando: {
        width: 300,
        left: 80,
        bottom:500,
    },
    //cloud
    cloud: {
        top:500
    }
});