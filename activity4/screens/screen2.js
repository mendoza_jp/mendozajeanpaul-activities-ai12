import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';


export default function Home({ navigation }) {
    const pressHandler = () => {
        navigation.navigate('Screen3')
      }
    
      const pressBack = () => {
          navigation.goBack();
      }

    return (
        <View style={styles.container}>
            <View style={styles.bg}>
                <View style={styles.circle2}/>
                <View style={styles.circle1}>
                    {/* BACK ICON */}
                    <TouchableOpacity style={{left:120,top:150,}} onPress={pressBack}>
                    <Icon name="arrow-back-ios" size={30} color="white" />
                    </TouchableOpacity>
                </View>
                <View style={styles.circle3}/>
                <View style={styles.circle4}/>
                <View style={styles.imgg}>
                <Image source={require('../assets/screen1_img.png')}
                    style={{width:300,height:250,right:10}}
                />
                </View>
            </View>
            <Text style={styles.title}>Technology is the future</Text>
            <Text style={styles.description}>It is inevitable. Technology will get better and better from here. The IT course will give me an edge in the future.</Text>

            {/* CONTINUE BUTTON */}
            <View>
                <TouchableOpacity style={styles.continue} onPress={pressHandler} >
                <Text style={styles.cont}>CONTINUE</Text>
                </TouchableOpacity>
            </View>
            
        </View>
        
    )
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: '#fefaff',
    },
    continue: {
        marginLeft: 30,
        height:20,
        width:150,
        color: '#FFF',
        borderColor: 'black',
        borderWidth:1,

    },
    title: {
        fontSize: 25,
        left:80,
        fontWeight:'bold',
        bottom:1800,
    },
    description: {
        opacity: 0.7,
        backgroundColor: 'white',
        fontSize: 16,
        left:80,
        fontWeight:'bold',
        bottom:1800,
        color: 'black',
        width: 300,
        borderRadius: 5
    },
    //background
    bg:{
        
    },
    //circles in bg
    circle1: {
        height: 300,
        width: 300,
        backgroundColor: '#473361',
        borderRadius:150,
        right:100,
        bottom:600
    },
    circle2:{
        height: 500,
        width: 500,
        borderRadius:500,
        bottom:340,
        left:100,
        backgroundColor: '#846da1',

    },
    circle3:{
        height: 600,
        width: 600,
        borderRadius:500,
        bottom:340,
        left:100,
        backgroundColor: '#ab95c7',
    },
    circle4:{
        height: 600,
        width: 600,
        borderRadius:500,
        bottom:800,
        right:320,
        backgroundColor: '#5E548E',
    },
    imgg:{
        alignContent:'center',
        justifyContent:'center',
        alignItems:'center',
        bottom: 1800,
    },
    //button
    continue: {
        bottom:1750,
        left:240,
        height:50,
        width:125,
        backgroundColor: '#231930',
        borderColor: 'black',
        borderWidth:1,
        borderRadius: 35,
    },
    cont: {
        alignItems: 'center',
        flex: 1,
        top: 13,
        marginLeft: 27,
        fontWeight:'bold',
        color: 'white'
    }
});